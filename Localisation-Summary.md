This summary was written in March 2020 with a mixture of existing and new documentation produced by the team. References have been given where applicable. 

# Hardware
## Computation Stack [1]

The **Computation Stack** is the collection of on-board computers. All computers on the computation stack communicate to each other over Ethernet using ROS. The Network Switch facilitates this communication. The computation stack serves as the brain and nerves of the whole system. At a very high level, it serves the following functions:
* Processing of sensory input data to perceive and understand the outside world
* Decision making - what needs to be done? Is it doing it?
* Control - make the car do it.

Currently, the Computation Stack comprises a few different types of computers used for different tasks:
Nvidia Drive PX2 : This is the primary on board Linux computer that runs Autoware. It runs all high level software functions (and also currently drives the braking system - this may change in the near future). It is in fact two systems in one that can communicate with each other very effectively.
* **Dell Optiplex**: This general purpose Linux computer serves as a controller for the GPS and IMU. It also drives the gear shifting actuation.
* **Raspberry Pi 3**: This runs a modified version of Ubuntu with ROS. It drives the steering actuation using a CAN BUS shield module (see Actuation Systems)
* Three **Arduino microcontrollers** for low level actuation control. (see Actuation Systems)
* **External laptop(s)**: While these are not considered part of the on-board computation stack, they can be added at will to the ROS network simply by plugging into the network switch. For the 2019 EOY demo, a laptop provided crucial monitoring information to the Chief Engineer who was seated in the passenger seat.

Other MCAV Computers:
* **The Beast**: A high performance development workstation that lives in the MCAV workshop. Used for developing, testing, simulating etc. 

## Perception Hardware [2]
MCAV currently has two vehicles in use - a Subaru Forester and StreetDrone. Some details, including sensors, and usage restrictions of each are described below.
### Subaru
![Subaru Forester](uploads/cc7512891b9c65ad1e7d8b141bd4327e/image19.jpg)
* Modified for autonomous driving purposes - gear shift actuation, pedal actuation and steering actuation (airbag removed), sensor mounting
* Located at a warehouse used by the team at 10 Redwood Drive, Notting Hill 
  * Due to the modifications to the vehicle it cannot be driven on public roads (only within the warehouse area)
  * It cannot be currently used for data collection 

Current (2020) Sensors:

| Sensor | Type | Amount | Mount | Connection | Website |
| ------ | ----- |------ |------ | ------ | ------ |
| Cameras | GMSL Sekonix | 2 | Roof Rack (Front/Back) | Fakra Z (PX2) | http://sekolab.com/products/camera/ |
| Lidar   | Velodyne VLP16-HiRes| 1 | Roof Rack (Mid) | Ethernet (Switch)| https://velodynelidar.com/products/puck-hi-res/ |
| GPS     | Piksi Multi| 1 | Roof Rack (Mid) | Ethernet (Switch) | https://www.swiftnav.com/piksi-multi |

Available (Unused) Sensors:

| Sensor | Type | Amount | Mount | Connection | Website |
| ------ |-----| ------ |------ | ------ | ------ |
| IMU | VectorNav VN-100 | 1 | Computation Rack | ?? | ?? | 
| Stereo Camera | ZED | 1 | N/A | USB | https://www.stereolabs.com/zed/ |
| Camera |GMSL Sekonix | 2 | N/A | Fakra Z | http://sekolab.com/products/camera/ |
| Lidar | SICK 2D | 2 | N/A | ?? |https://www.sick.com/au/en/detection-and-ranging-solutions/2d-lidar-sensors/lms5xx/c/g179651 | 

### StreetDrone
![StreetDrone Twizy](uploads/d9496d8c543335b55a7852a0fa2761b5/image11.jpg)
* Drive by wire, electric vehicle with sensors pre-installed
* Located at the team’s workshop at Engineering 37 (17 Alliance Lane, Clayton)
  * Can currently only be driven within the Monash University campus
  * The team is in the process of getting approval from VicRoads for use on public roads - time of completion is unknown 

Current Sensors:

| Sensor | Type | Amount | Mount | Connection | Website |
| ------ | ------ |------ | ------ |------ | ------ |
| Camera | GMSL Sekonix | 8 | In Built | Fakra Z (PX2) | https://www.streetdrone.com/ |
| GPS and IMU  | PCAN-GPS | 1 | In Built | CAN | https://www.peak-system.com/PCAN-GPS.378.0.html?&L=1 |
| Ultrasonics | Neobotix ultrasonic sensors | 8 |In Built | CAN |https://www.neobotix-robots.com/products/robot-components/usboard| 
| Lidar   | Velodyne VLP16-HiRes| 1 | Roof (Mid) | Ethernet (PX2)| https://velodynelidar.com/products/puck-hi-res/ |

## Software
![Software stack](uploads/e087df8b550b96c2f36d3998ea658ab4/image12.png)*Software stack*

Software versions:
* Autoware 1.12
* ROS Kinetic
* Python 3.6
* Ubuntu 16.04

The team currently uses Autoware - an open source software for autonomous vehicles which contains a collection of ROS packages that implement autonomous driving functionality. Autoware provides a runtime manager to launch, and control a collection of ROS nodes. We currently use a mixture of Autoware.AI packages for sensors, localisation, path planning, and control. We use these packages in conjunction with software we have written ourselves.

![Autoware interface](uploads/821ecb10499d56cb514485af1790c7a7/image9.png)*Autoware interface*

### Data
The team has several ROS bags of data recorded on both the Subaru and StreetDrone. A short summary is below with images of each topic. 

Locations:
* Research Way (LiDAR, GPS, IMU, camera)
* OC1 Car park (LiDAR, GPS, IMU)
* Warehouse (LiDAR, GPS, IMU)
* Workshop to Research Way (LiDAR, GPS, camera)
#### Subaru
Topics in bag:
* LiDAR: /points_raw
* GPS: /fix
* IMU: /imu/data, /imu/mag
* Front camera: /gmsl_camera/port_0/cam_0/image_raw
* Rear camera: /gmsl_camera/port_0/cam_1/image_raw

![Rqt_bag of IMU, GPS and camera data](uploads/6b8f4578217686420532def2f755e48d/image8.png)*Rqt_bag of IMU, GPS and camera data*

![Rviz of LiDAR and camera data](uploads/570e69c529f39f06fd3c1b2e1a425830/image17.png)*Rviz of LiDAR and camera data*

#### StreetDrone
Topics in bag:
* LiDAR: /points_raw
* Front camera: /gmsl_camera/port_0/cam_0/image_raw
* GPS, IMU and Ultrasonics: CAN messages
  * These messages are converted using a python script
  * GPS data - latitude, longitude and altitude (currently no covariance)
  * IMU data - angular velocity, linear acceleration and magnetic field (currently no covariance and no orientation determined)
  * Ultrasonics data - field of view, range

![Rviz of LiDAR and camera data](uploads/7f3f705efdc0b7fae1c9b488d1782139/image13.png)*Rviz of LiDAR and camera data*

![GPS script and topic output](uploads/0fd26d0a3a7ca8eec3db8ce0de80c0cf/image15.png)*GPS script and topic output*

![IMU script and topic output](uploads/b0d61bafa4454d9a3f0e9c4a8df2712a/image21.png)*IMU script and topic output*

![Ultrasonic script and topic output](uploads/53c457b2763cfc47a84b36a050532491/image20.png)*Ultrasonic script and topic output*

### ROS Nodes
![Implementation of the mapping and localisation nodes currently used as part of Autoware](uploads/473d9efb0492cb380d838f96ab0d3fcc/image22.png)*Implementation of the mapping and localisation nodes currently used as part of Autoware*

### NDT[3]
NDT utilises normal distributions to match the point cloud and map by describing the probability of finding part of the surface at any point in space. There are both 2d and 3d variants of this algorithm. In the 2d form, space is subdivided into constant and regularly sized cells. Each cell contains at least three points and the following procedure is followed: 
1. Collect all 2D-Points $`x_{i-1...n}`$ contained in this box. 
1. Calculate the mean $`q=\frac{1}{n}\sum_ix_i`$
1. Calculate the covariance matrix $`\sum=\frac{1}{n}\sum_i(x_i-q){(x_i-q)}^T`$
  
The  probability of measuring a sample at 2D-point x contained in this cell is now modeled by the normal distribution N(q,): $`p(x)\text{\textasciitilde}exp(-\frac{{(x-q)}^T\sum^{-1}(x-q)}{2})`$


When this is extended to 3d, space is instead split into a cubic lattice called voxels. When using NDT in 3d, cell size is extremely important as if it is too large, smaller features will be blurred out but if it is too small, scans may be too close together for the algorithm to succeed. Smaller cell size will also require more memory. Since normal distributions give a piecewise smooth representation of the point cloud, it is possible to apply standard numerical optimisation methods to improve its accuracy. In comparison to ICP, NDT is considered more robust with lower computational cost but when it fails it will fail more drastically in translation and rotation whereas ICP mainly fails in translation. 

Autoware includes both ICP and NDT algorithms for scan matching but NDT is chosen for its robustness and efficiency over ICP.

### Mapping process [4]
Autoware has an in-built package for generating a 3d point cloud map from pre-recorded ROS bags. The initial scan is set as a foundation of the map, and its pose determines the pose of the entire map. The next step is to find the best estimate of the pose between the previous and current scan. On the previous scan, NDT is applied, while the current scan is filtered with Voxel Grid Filter to increase computational performance. To find the best estimate of the pose, Newton’s Line Search Algorithm is used, since the solution lies in the minimum of score function: 

$`s(p)=-\sum^n_{k=1}p(T(p,x_k))`$

Once the algorithm converges, the solution in terms of the pose is used to transform the current scan and the result is added to the previous scan. 

![NDT Mapping](uploads/36f5994e1083333c50a576a750d2862c/image6.png)*NDT Mapping*

The output of the NDT mapping algorithm is a 3D point cloud map without position or orientation in a global coordinate frame, instead a map frame is created, with its origin where the car is located at the beginning of the recording. 

![Location of map coordinate frame origin](uploads/cef70603697dc3d3f574cc8a2f6ff8e5/image10.png)*Location of map coordinate frame origin*

The map coordinate frame acts as a parent to the GPS and Velodyne frames. Autoware has two inbuilt nodes for converting the GPS coordinates (latitude, longitude, altitude) into cartesian coordinates (X, Y, Z). In our case, GPS data was stored in /fix topic with NMEA information, so the selected node was /fix2tfpose. This node takes only a plane number as input. There are 19 different planes available and they are part of the Japanese Rectangular Coordinate System. The origins of the 19 planes are shown below. 

![Origins of planes in Japanese Coordinate System](uploads/db2b2067d59ffe459586e64848162ed1/image18.png)*Origins of planes in Japanese Coordinate System*

An additional 20th plane was created with origin coordinates at 145 degrees E 38 degrees S which covers the Monash campus. The team has plans to convert into geocentric coordinates in the future. 

The origin of the map coordinate frame is transformed to the origin of the newly created 20th plane. The initial GPS position from the ROS bag used to generate the map is recorded from the /fix2tfpose node (so it is relative to the 20th plane). The point cloud is transformed using the translation. 

![Origin of map coord frame](uploads/1c1be2c6348fd8ee629d509aa44254cd/image16.png)*Corrected position of the origin of map coordinate frame
(Note in the upper right-hand corner 3D point cloud map)*

### Scan matching
Scan matching is very similar to the mapping process except rather than comparing the current scan with previous, it is compared with reference 3D point cloud map. The result of the scan matching process is the pose of the vehicle in a global coordinate frame. This process uses GPS as an initial guess of position. The IMU was not in use at the time so no accurate measure of orientation was possible. We have localised using recorded ROS bags in the maps we have generated as well as live. 

![NDT Scan Matching](uploads/447a5972c2512d488541a52648becf0a/image7.png)*NDT Scan Matching*

![Rviz visualisation of localisation](uploads/4569b26f50de70b91326eb8f352a98a7/image14.png)*Rviz visualisation of localisation*

There has been development by the creators of Autoware on a SLAM package using their existing algorithms which is available, however, the team has not tested or used this code. The team is also trying to move away from using the Autoware packages and write its own due to unwanted features and further progression and understanding of code in the future. 

# Related Work
## Sensor Fusion
An FYP was completed last year using an Extended Kalman Filter to fuse IMU, GPS and LiDAR data together. An open source package (robot_localization) was tested on recorded ROS bags but never live. 

## Visual Odometry 
Research is being undergone by other members of the team in monocular depth estimation and visual odometry with stereo cameras. Monodepth2 for Ubuntu 18.04 is currently being researched for monocular depth estimation. A pipeline was also built to take two camera inputs and generate a depth map and point cloud using inbuilt ROS packages.

# References
[1] A.Salter, ‘Computation Stack’, 2020. [Online]. Available: https://git.infotech.monash.edu/MonashCAV/get-started/wikis/hardware/Computation-Stack

[2] P. Cleeve, A.Salter, ‘Computation Stack’, 2020. [Online]. Available: https://git.infotech.monash.edu/MonashCAV/get-started/wikis/hardware/Perception-Systems

[3] R. Deo, Y. Ho, S. Petrovic, “Final Research Report - Localisation,” Melbourne, 2018. https://docs.google.com/document/d/19rNhkWJq1XEV6pc7PISo8P_ENrsJHQ3PSmFYEbD0cYA

[4] S. Petrovic, "Evaluation of Normal Distribution Transform Parameters in Mapping and Localisation Algorithms," Melbourne, 2019.
https://drive.google.com/drive/folders/1gJLDINChSwx_c6TpVZMmiGCnplMhwx-V
