# Existing Neural Networks for estimating depth
* DORN: https://github.com/hufu6371/DORN
  * Paper: https://arxiv.org/abs/1806.02446
  * Implementation: Caffe 
* VNL Monocular depth prediction: https://github.com/YvanYin/VNL_Monocular_Depth_Prediction
  * Paper: https://arxiv.org/abs/1907.12209
  * Implementation: Pytorch
* SemiDepth: https://github.com/jahaniam/semiDepth
  * Paper: https://arxiv.org/pdf/1905.07542.pdf
  * Implementation: Tensorflow, based on monodepth
* MultiDepth: https://github.com/lukasliebel/MultiDepth
  * Paper: https://arxiv.org/abs/1907.11111
  * Implementation: Pytorch
* DenseNet: https://github.com/liuzhuang13/DenseNet
  * Pytorch implementation: https://pytorch.org/docs/master/torchvision/models.html
* monoResMatch: https://github.com/fabiotosi92/monoResMatch-Tensorflow
  * Paper: https://vision.disi.unibo.it/~ftosi/papers/monoResMatch.pdf
* Visualisation of …: https://github.com/JunjH/Visualizing-CNNs-for-monocular-depth-estimation
  * Paper: http://openaccess.thecvf.com/content_ICCV_2019/papers/Hu_Visualization_of_Convolutional_Neural_Networks_for_Monocular_Depth_Estimation_ICCV_2019_paper.pdf
  * Implementation: Pytorch
* DenseDepth: https://github.com/ialhashim/DenseDepth
  * Paper: https://arxiv.org/abs/1812.11941
  * Implementation: Tensorflow 
* DiverseDepth: https://github.com/YvanYin/DiverseDepth
  * Paper: https://arxiv.org/abs/2002.00569
  * Implementation: Pytorch
* MiDAS: https://github.com/intel-isl/MiDaS
  * Paper: https://arxiv.org/pdf/1907.01341v2.pdf
* BTS: https://github.com/cogaplex-bts/bts
  * Paper: https://arxiv.org/abs/1907.10326
  * Implementation: Pytorch and Tensorflow
  * Notes 
    * Supervised monocular depth estimation network w/ novel local planar guidance layer
    * CNN approach using ResNet-101, ResNext-101 and DenseNet-161 for dense feature extraction 
    * Reduce feature map resolution to H/8 then back to H
    * Rather than nearest neighbour up-sampling, use local planar assumption to guide features to full resolution 
