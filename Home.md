This wiki is to document the MCAV FYP with a focus on SLAM (Simultaneous Localisation and Mapping). 

Some key focus areas relevant to the project are:
*  Moving away from the use of Autoware - this could mean writing code from scratch or refactoring to better suit the team's purposes and needs
*  Moving to a geocentric coordinate system, currently a Japanese rectangular coordinate system is used
*  Utilising IMU data for orientation purposes (may or may not be included)
*  Developing a SLAM algorithm that utilises both camera and LiDAR data
   *  Fusing camera and LiDAR data to create training depth maps and training a neural net to generate depth maps from images
   *  Mainly focusing on visual SLAM algorithms 


**Contents**

*Research & Updates*
* [Localisation Summary](./Localisation-Summary)
* [Meeting Summaries](./Meeting-Summaries)
* [Literature Review](./Literature-Review)
* [Neural Network Research](./Neural-Network-Research)

*Documentation*
* [Depth Labelling](./Depth-Labelling)
* [Depth Estimation Neural Network](./Depth-Estimation-Neural-Network)
* [ORB-SLAM2](./ORB-SLAM2)
* [Docker](./Docker)