**Table of Contents**

[[_TOC_]]

# Plan
* Look into existing neural networks for depth estimation
* Start writing a basic convolution neural network from scratch
* Implement custom loss function
* Iteratively improve neural network 

# Progress

## Update 23/4/20
* Implemented an existing neural network on the Beast - [BTS ](https://github.com/cogaplex-bts/bts) with the KITTI Eigen split dataset
* Virtual Environment (anaconda3): Python 3.6, PyTorch 1.2.0, Torchvision 0.4.0, Cuda 10.0
* Had issues with the Dataloader hanging when multiprocessing 
  * Changing `num_workers=1` to `num_workers=0` stopped hanging (is an issue that has been raised on forums/[git](https://github.com/pytorch/pytorch/issues/1355) in the past, may be system requirements/ settings)
    * This "fix" decreases processing speed of the neural network
  * No other member of the team is currently using `num_workers>0` with their neural nets on the same machine (will ask around further)

Computing errors (697 validation images):
*  Using DenseNet-161 
```console
     d1,      d2,      d3,  AbsRel,   SqRel,    RMSE, RMSElog,   SILog,   log10
  0.955,   0.993,   0.998,   0.060,   0.249,   2.798,   0.096,   8.933,   0.027
```

Test image (input then depth output):
![0000000000](uploads/45d4b39187fd09a60c2554353e57661b/0000000000.png)
![2011_09_26_drive_0002_sync_0000000000](uploads/291c64a7d3a16c67212364b509e2dcaa/2011_09_26_drive_0002_sync_0000000000.png)

## Update 30/4/20
* Created a separate [Depth Estimation repository](https://git.infotech.monash.edu/MonashCAV/software/depth-estimation)
  * Another team member is also looking into depth estimation so we will be working on separate branches
* Planning to use BTS as a base
  * Will likely remove unnecessary parts of the code/ simplify where possible
* Begun working on pre-processing images
  * KITTI images (1242 x 375) and StreetDrone images (480x302) are different sizes 
  * BTS currently crops KITTI images to 1216x352 (multiples of 32 to account for strided convs and upconv layers)
  * In its current form, StreetDrone images cannot be inferred or used to train the neural network
* Attempted use a higher num_workers but DataLoader still hung
* Moving forward will start from a simpler network as it is difficult to tell what design decisions were made in the BTS network to modify for my project purposes 

## Update 4/5/20
Current proposed architecture for a simple CNN for depth estimation:
![Localisation_diagrams_-_Network_Architecture__1_](uploads/c65e3ca1ac047530078c25ddb67ce005/Localisation_diagrams_-_Network_Architecture__1_.png)

Plans:
* Implement above architecture in PyTorch - performance will be limited at the moment
* Iteratively improve NN from this base
* Introduce more downsampling via either maxpool or strided convs and upsampling via transpose convs to increase the size of the receptive field

## Update 8/5/20
* Implemented simple CNN for depth estimation
  * Ran with one epoch
  * Loss: log MSE over pixels with ground truth data
* Currently no statistics to measure performance

Test image (input then depth output):
![0000000069](uploads/9cb746bb29cc57fc11a63a4425554e1b/0000000069.png)
![predicted6](uploads/cb907e51b30844371372f078a78f3abf/predicted6.png)

## Update 12/5/20
* Added error calculations
  * $`D_i`$ is predicted depth & $`D_i^*`$ is ground truth depth
  * $`SILog:\frac{1}{n}\sum_id_i^2-\frac{1}{n^2}(\sum_id_i)^2, d_i=\log(D_i)-\log(D_i^*)`$
  * $`Rel_{abs}:\frac{1}{n}\sum^n_{i=1}|\frac{D_i-D_i^*}{D_i^*}|`$
  * $`Rel^2:\frac{1}{n}\sum^n_{i=1}\frac{(D_i-D_i^*)^2}{D_i^*}`$
  * $`Log10:\frac{1}{n}\sum^n_{i=1}|\log_{10}(D_i)-\log_{10}(D_i^*)|`$
  * $`RMS:\sqrt{\frac{1}{n}\sum^n_{i=1}|D_i-D_i^*|^2}`$
  * $`RMS_{log}:\sqrt{\frac{1}{n}\sum^n_{i=1}|d_i|^2}`$
  * % of points within $`\delta:\frac{1}{n}\sum^n_{i=1}max(\frac{D_i}{D_i^*},\frac{D_i^*}{D_i})<\delta, \delta=1.25m`$
* Changed colour map to jet

Configuration:
* Batch size = 4
* Learning rate = 1e-4
* Epochs = 1 
* Height, width = 352, 1216

Errors for current model (used KITTI Eigen split for training and testing):
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
49.1818,  0.1820,   0.478,   5.094,  10.753,   0.524,   0.340,   0.617,   0.802
```

Test image (input then depth output):
![0000000069](uploads/9cb746bb29cc57fc11a63a4425554e1b/0000000069.png)
![predicted0](uploads/64cd0ba91952c21575a7a6c2497c8dbe/predicted0.png)

## Update 14/5/20
* Added a config file
* Changed architecture/model

### Architecture
Implemented a U-net structure architecture with existing base. 

Layer types:
-   Down
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
    -   Conv 4x4, padding 1, stride 2
    -   BatchNorm
    -   ReLU
-   Up
    -   TransposeConv 4x4, padding 1, stride 2
    -   BatchNorm
    -   ReLU
    -   Concatenation
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
-   Middle
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
-   Final
    -   Conv 3x3, padding 1, stride 1
    -   Exponential

| **Layer type** | **Resolution (in, out)** | **Channels (in, out)** |
|----------------|--------------------------|------------------------|
| Down           | 352x640, 176x320         | 3, 32                  |
| Down           | 176x320, 88x160          | 32, 64                 |
| Down           | 88x160, 44x80            | 64, 128                |
| Middle         | 44x80, 44x80             | 128, 256               |
| Up             | 44x80, 88x160            | 256, 128               |
| Up             | 88x160, 176x320          | 128, 64                |
| Up             | 176x320, 352x640         | 64, 32                 |
| Final          | 352x640                  | 32, 1                  |

### Training/Testing
Configuration:
* Batch size = 10
* Learning rate = 1e-4
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

Errors for current model (used KITTI Eigen split for training and testing):
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
22.1820,  0.0744,   0.190,   1.290,   5.225,   0.243,   0.741,   0.917,   0.973
```

Test image (input then depth output):
![0000000416](uploads/cfdf4236e3370cac0ded37c83c8c08f9/0000000416.png)
![predicted648](uploads/8e01aefb20408d720709b8ce33bf11d7/predicted648.png)

### StreetDrone testing
* Output height x width = 288 x 480

Errors for current model (random StreetDrone data without adjusting for focal length):
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
47.4403,  0.3898,   1.799,  56.440,  26.951,   1.003,   0.088,   0.193,   0.332
```

Test image (input then depth output):

![cam_rectified86](uploads/e488c445895076e40c51018d9ae58f1a/cam_rectified86.png)
![predicted255](uploads/5e9afd7be43d726774b1ba86ece9e4e6/predicted255.png)

## Update 18/5/20
### Downsample experiment
Removing one convolutional layer from each downsample
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
25.0320,  0.0913,   0.253,   1.986,   5.863,   0.289,   0.658,   0.878,   0.954
```
![down_predicted648](uploads/ec9787e4fb4a0893a6d8e708980daace/down_predicted648.png)

Error maps:
![unet_downsample_error_map648](uploads/31a483d6c2dbef4d733afc58587ca5dc/unet_downsample_error_map648.png)
![unet_downsample_combined_map648](uploads/d7fe5d14d93f2db6b09ae1608a434f42/unet_downsample_combined_map648.png)

### Upsample experiment
Removing one convolutional layer from each upsample
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
25.8847,  0.0905,   0.250,   1.963,   5.921,   0.288,   0.672,   0.875,   0.952
```
![up_predicted648](uploads/061cc6d8cbfaad077dc27524a3e992ac/up_predicted648.png)

Error maps:
![unet_upsample_error_map648](uploads/8a850a09a20d6a13113ecb240208544f/unet_upsample_error_map648.png)
![unet_upsample_combined_map648](uploads/cd08f57411933f344a5b88b70999629c/unet_upsample_combined_map648.png)

* The effect on performance is less prominent in the upsample than the downsample - potentially the capacity could be reduced by simplifying the upsample layers
* Larger errors occurred in the same areas - for objects further away

## Update 20/5/20
Added error mapping in (including combination with original image)
*  Have added in error maps for previous experiment 

U-net error map:
![error_map648](uploads/17a7078571ca7cbb6e8e88fd118237e0/error_map648.png)
![combined_map648](uploads/b6ecf7f0120ede09ee0cb5d86a51206f/combined_map648.png)

## Update 21/5/20
Added training parameter for focal length offset (with original U-net structure). This resulted in decreased performance overall but it was possible to get some values for the StreetDrone data after continued training - the loss function has not been updated to account for error in lining up LiDAR and camera data. 

![NetworkArchitecture-Unet_focal](uploads/7c7beb46fc28afad549287b628f33ce1/NetworkArchitecture-Unet_focal.png)

The final layer changed to:
* Conv 3x3, padding 1, stride 1 + Trainable offset parameter
* Exponential

### KITTI dataset
Configuration:
* Batch size = 10
* Learning rate = 1e-4
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

Results:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
23.6288,  0.0843,   0.173,   1.293,   6.514,   0.275,   0.694,   0.889,   0.961
```

Prediction:
![focal_predicted648](uploads/0874f093dbf2b13534caea81a28ea374/focal_predicted648.png)

Error maps:
![unet_focal_error_map648](uploads/a74e4ff2406b093c14a87ca4a34a577c/unet_focal_error_map648.png)
![unet_focal_combined_map648](uploads/a40afe922c5019df5f19b0811dfc8ea5/unet_focal_combined_map648.png)

### StreetDrone dataset
Trained KITTI model with StreetDrone data (full dataset excluding 2020-02-24-14-12-34 which was used for testing)

Configuration:
* Batch size = 12
* Learning rate = 1e-4
* Epochs = 5 
* Input height x width = 288 x 480
* Output height x width = 288 x 480

Results:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
35.5307,  0.1173,   0.278,   3.158,   9.547,   0.370,   0.570,   0.808,   0.912
```

Prediction:

![sd_focal_predicted640](uploads/5ec1790b9d3d1e676bd6743754bc9704/sd_focal_predicted640.png)

Error maps:

![error_map640](uploads/2ed9ffbae7fe448b6530d0fdb96abd23/error_map640.png)
![combined_map640](uploads/0702d5b6b4260f8d2a28bfb5803f9be6/combined_map640.png)

### Model checkpoint performance
Best performance on last epoch for original U-net

Better performance on 3rd epoch for downsample experiment:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
25.7598,  0.0850,   0.208,   1.496,   6.053,   0.276,   0.693,   0.888,   0.959
```

Better performance on 4th epoch for upsample experiment:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
23.7356,  0.0782,   0.187,   1.241,   5.594,   0.255,   0.724,   0.908,   0.968
```
*  The discrepancy in performance to the original u-net structure is not extremely large - more reason to potentially change the architecture

Better performance on 4th epoch for u-net w/ focal length (KITTI):
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
23.0640,  0.0757,   0.172,   1.131,   5.417,   0.248,   0.737,   0.913,   0.971
```
*  This performance is quite similar to the original u-net structure
*  Best performance on last epoch for u-net w/ focal length (StreetDrone)
*  Tried continuing training with StreetDrone data on this checkpoint instead of last checkpoint and it performed worse
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
36.4010,  0.1205,   0.297,   3.484,   9.816,   0.378,   0.555,   0.797,   0.908
```

## Update 28/5/20
### Overfitting issue
Overfitting was present as seen by earlier epochs performing better. This still occurred with weight decay 1e-2 added but less so than before.

Upsample (less) 4th epoch:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
23.3703,  0.0775,   0.187,   1.226,   5.460,   0.251,   0.728,   0.909,   0.969
```

Upsample (less) 5th epoch:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
24.4571,  0.0802,   0.181,   1.259,   5.883,   0.265,   0.717,   0.897,   0.963
```

All changes to fix overfitting:
* Weight decay 1e-2
* Input data changes
  * Random cropping 
  * Flipping
  * Colour jitter to image

Have not seen same issue recur yet after above changes.

### Modified architecture
![NetworkArchitecture-Current](uploads/3fcd271b219c9b1a2cd4577e53840096/NetworkArchitecture-Current.png)

Layer types:
-   DoubleConv_x
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
    -   Conv 3x3, padding 1, stride 1
    -   BatchNorm
    -   ReLU
-   Down_x
    -   Conv 4x4, padding 1, stride 2
    -   BatchNorm
    -   ReLU
-   Depth_x (lowest level)
    -   Conv 3x3, padding 1, stride 1
-   Up_x
    -   TransposeConv 4x4, padding 1, stride 2
    -   BatchNorm
    -   ReLU
-   Depth_x
    -   Interpolate(Depth_x-1, scale factor = 2) + Conv3x3(concatenate(Up_x, DoubleConv_x)))
-   Final
    -   Depth0 + offset
    -   Exponential

|    **Layer type**       |    **Resolution (in, out)**|    **Channels (in, out)**|
|-------------------------|----------------------------|--------------------------|
|    DoubleConv0          |    352x640, 352x640        |    3, 16                 |
|    Down1                |    352x640, 176x320        |    16, 16                |
|    DoubleConv1          |    176x320, 176x320        |    16, 32                |
|    Down2                |    176x320, 88x160         |    32, 32                |
|    DoubleConv2          |    88x160, 88x160          |    32, 64                |
|    Down3                |    88x160, 44x80           |    64, 64                |
|    DoubleConv3          |    44x80, 44x80            |    64, 128               |
|    Depth3               |    44x80, 44x80            |    128, 1                |
|    Up2(DoubleConv3)     |    44x80, 88x160           |    128, 64               |
|    Depth2               |    88x160, 88x160          |    1, 1                  |
|    Up1(concatenate2)    |    88x160, 176x320         |    128, 32               |
|    Depth1               |    176x320, 176x320        |    1, 1                  |
|    Up0(concatenate1)    |    176x320, 352x640        |    64, 16                |
|    Depth0               |    352x640, 352x640        |    1, 1                  |
|    Final                |    352x640                 |    1, 1                  |

#### Results
Configuration:
* Batch size = 28
* Learning rate = 1e-4
* Weight decay = 1e-2
* Epochs = 10 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

5 epochs:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
33.4540,  0.1141,   0.261,   2.345,   8.158,   0.362,   0.585,   0.811,   0.914
```

10 epochs:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
30.2595,  0.1004,   0.235,   1.981,   7.246,   0.324,   0.630,   0.846,   0.937
```
*  This architecture performed worse than all previous architectures (except the most basic), with the same number of epochs. Likely due to the adjusted channel numbers. The introduction of measures to reduce overfitting should not have effected the results as drastically.  

**Channels multiplied by 2**

Configuration:
* Batch size = 12
* Learning rate = 1e-4
* Weight decay = 1e-2
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
27.6313,  0.0946,   0.222,   1.619,   6.555,   0.296,   0.653,   0.872,   0.954
```

Prediction:
![interpdouble_predicted648](uploads/345cb7edc550bf13c790058fc1d4cfb4/interpdouble_predicted648.png)

Error maps:
![interpdouble_combined_map648](uploads/17245943273b491eba79701085eb44b0/interpdouble_combined_map648.png)
![interpdouble_error_map648](uploads/9cc95708ef67b8e9158f3e1fc73d1352/interpdouble_error_map648.png)

### Transfer Learning
Changed loss function
* $`D_i`$ is predicted depth & $`D_i^*`$ is ground truth depth
* $`Loss:\frac{1}{n}\sum_i\frac{d_i^2}{c^2+d_i^2}, d_i=\log(D_i)-\log(D_i^*), c=\log(0.05)m`$

Without fixing any parameters:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
37.3938,  0.1291,   0.291,   3.330,  10.450,   0.396,   0.517,   0.772,   0.896
```

Prediction:

![predicted640](uploads/ec03485fcce177ec1e1a35e9cba9d3f4/predicted640.png)

Error maps:

![combined_map640](uploads/872aed27800831ab779deb01adeb369c/combined_map640.png)
![error_map640](uploads/c1d2d96cbe06ffd4e7247dc2140a5c25/error_map640.png)

Fixed all parameters except offset for focal length:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
48.8417,  0.2061,   0.624,  10.763,  14.474,   0.574,   0.270,   0.524,   0.743
```

*  Performed worse than previous transfer learning

## Update 3/6/20
Unet, single conv on upsample 
* Potentially worse performance with augmentation overall 
```console 
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
26.1094,  0.0889,   0.202,   1.423,   6.364,   0.283,   0.682,   0.885,   0.959
```

Attempted without ColourJitter (potentially the values used were too extreme)
*  Performed even worse
*  Currently unsure what caused performance to drop - only difference in training was augmentation and renaming of model layers (training is not expected to yield exact same result each time due to random mini batches and augmentations)

## Update 8/6/20
Recalculated error statistics with same centre crop on datasets:

|     Architecture              |     SILog      |     Log10    |     Rel_abs    |     Rel_sq    |     RMS       |     Log   RMS    |     D1       |     D2       |     D3       |
|-------------------------------|----------------|----------------|--------------|---------------|---------------|------------------|--------------|--------------|--------------|
|     basic                     |     49.1329    |     0.1817     |     0.478    |     5.08      |     10.728    |     0.524        |     0.341    |     0.618    |     0.803    |
|     unet_kitti                |     22.1152    |     0.073      |     0.187    |     1.281     |     5.204     |     0.241        |     0.743    |     0.917    |     0.973    |
|     unet_focal_kitti          |     23.5558    |     0.083      |     0.171    |     1.285     |     6.505     |     0.274        |     0.697    |     0.89     |     0.961    |
|     unet_focal_sd             |     36.4655    |     0.1207     |     0.298    |     3.502     |     9.841     |     0.378        |     0.554    |     0.796    |     0.908    |
|     unet_focal_upsample_wd    |     24.3503    |     0.0789     |     0.178    |     1.247     |     5.86      |     0.264        |     0.719    |     0.898    |     0.963    |
|     sd_new_loss-3             |     37.3301    |     0.1289     |     0.29     |     3.318     |     10.42     |     0.395        |     0.518    |     0.772    |     0.896    |
|     sd_new_loss_fixed-4       |     49.8008    |     0.2079     |     0.628    |     10.813    |     14.259    |     0.578        |     0.273    |     0.518    |     0.731    |
|     current_architecture      |     27.596     |     0.0911     |     0.218    |     1.593     |     6.447     |     0.293        |     0.657    |     0.873    |     0.954    |
|     unet_up_comparison        |     26.2362    |     0.085      |     0.196    |     1.395     |     6.252     |     0.28         |     0.687    |     0.886    |     0.959    |

*  Fixed colour of combined error images
   *  CV2 writes images with BGR format rather than RGB

Fixed colours:
![combined_map648](uploads/f3d6650c8cc43b264f02690c6f7c91ce/combined_map648.png)

## Break Week 1
### Update 29/6/20
Tried to look into under-performance of new architecture
*  Three tests (training from scratch with data augmentation)
   1. U-net (with focal adjustment) - new layer names
   1. Unet with one less conv for upsampling - old layer names (to see if an error was caused here)
   1. Current architecture with an extra convolution step
*  Considering reducing training dataset to speed up time (takes ~2hours for 5 epochs)

Configuration:
* Batch size = 10
* Learning rate = 1e-4
* Weight decay = 1e-2
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

| Architecture                  | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|-------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| unet_focal_new                | 25.4558 | 0.0847  | 0.19  | 1.337 | 6.117 | 0.278   | 0.687 | 0.888 | 0.96  |
| unet_focal_upsample_new       | 26.8913 | 0.0896  | 0.203 | 1.483 | 6.476 | 0.291   | 0.665 | 0.876 | 0.955 |
| current_architecture_modified | 26.9447 | 0.0904  | 0.206 | 1.532 | 6.528 | 0.293   | 0.664 | 0.873 | 0.952 |

*  Values all quite similar, unet with focal adjustment worse than previously. Random batches could be effecting values/ addition of augmentation. 
*  Will use these values as a comparison for improvement 
*  Will try reducing complexity of architecture modification on upsampling (only on the last layer or similar)

### Update 30/6/20
* Reduced complexity of architecture to include interpolation only on the last layer
  *  Repeated with interpolation on last two layers
* Trained unet_focal_new for 5 more epochs (10 in total) and achieved results better than those without augmentation
  * Augmentation and weight decay resulted in slower learning (but previous overfitting issues do not occur)

| Architecture               | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|----------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_final_layer         | 28.5178 | 0.0924  | 0.201 | 1.54  | 6.568 | 0.307   | 0.669 | 0.869 | 0.944 |
| unet_focal_new (10 epochs) | 23.4532 | 0.0752  | 0.171 | 1.105 | 5.322 | 0.25    | 0.735 | 0.91  | 0.969 |
| interp_last_2_layers       | 27.0147 | 0.0883  | 0.2   | 1.459 | 6.13  | 0.29    | 0.679 | 0.881 | 0.953 |

### Update 1/7/20
Tried batch size of 12 for interp_last_2_layers with 5 epochs and 10

| Architecture                     | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|----------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_last_2_layers   (bs12)    | 26.3288 | 0.0869  | 0.215 | 1.661 | 6.063 | 0.282   | 0.678 | 0.882 | 0.959 |
| interp_last_2_layers (10 epochs) | 23.6833 | 0.0771  | 0.175 | 1.151 | 5.64  | 0.255   | 0.726 | 0.905 | 0.968 |

Started implementation of a different loss function for transfer learning
* Window around each of these points in the predicted value
  * For each value in window calc loss (log difference, squared) and then find min of all these
  * Min value is the loss for that pixel
* Mask values - could not mask earlier due to neighbourhood nature of search

### Update 2/7/20
Testing of new loss function
* Initially did not work due to NaN values in backpropagation - added in epsilons to account for zeros before taking log
  * To debug where issues were occurring used [anomaly detection](https://pytorch.org/docs/master/autograd.html#anomaly-detection)
* Conducted from interp_last_2_layers (10 epochs) with no frozen layers and all fixed except focal offset parameter
* Metrics as they stand perform even worse (expected since they rely on perfect groundtruth - require a jitter to calculate accuracy here?) 

| Architecture    | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|-----------------|---------|---------|-------|-------|--------|---------|-------|-------|-------|
| interp_sd       | 60.0727 | 0.2031  | 0.509 | 9.306 | 14.683 | 0.611   | 0.337 | 0.589 | 0.753 |
| interp_sd_fixed | 52.3131 | 0.1971  | 0.507 | 6.612 | 12.89  | 0.55    | 0.278 | 0.543 | 0.772 |
*  Better performance for most metrics when only focal offset is trainable 

Results (no fixed layers)

![predicted640](uploads/3ff9ec0fb14f582465e278e56c439c0a/predicted640.png)
![combined_map640](uploads/3acb304e45e7b57a26b5987b5a2b07f9/combined_map640.png)

Results (fixed)

![predicted_interp_sd_fixed](uploads/705ec4528361906eac9d9003a69d3099/predicted_interp_sd_fixed.png)
![combined_interp_sd_fixed](uploads/dd6ffccbde9b976a63761dcb8e8259fe/combined_interp_sd_fixed.png)

### Update 3/7/20
Transfer learning
* Tried training focal offset and and layer 0 and layer 1 (including downsampling) - performed better than other two - seems like the better way to go, will change when downsampling architecture is modified 
* Tried fixing layer 0 and focal offset - performed worse than last test

| Architecture               | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|----------------------------|---------|---------|-------|-------|--------|---------|-------|-------|-------|
| interp_new (10   epochs)   | 24.5463 | 0.0798  | 0.187 | 1.205 | 5.468  | 0.259   | 0.712 | 0.906 | 0.968 |
| interp_sd_fixed_first      | 45.1683 | 0.1583  | 0.362 | 4.957 | 11.908 | 0.477   | 0.425 | 0.695 | 0.845 |
| interp_sd_fixed_very_first | 45.8905 | 0.1657  | 0.388 | 5.004 | 12.118 | 0.481   | 0.369 | 0.667 | 0.842 |

Results (fixed - offset, layer 0 and layer 1)

![predicted_interp_fixed_first](uploads/d972dbc73a2c2006b57e9c3488f47fb4/predicted_interp_fixed_first.png)
![combined_interp_fixed_first](uploads/9a4ac843c2c468aa763a516aa008ed20/combined_interp_fixed_first.png)

## Break Week 2
### Update 6/7/20
* Tried using bicubic interpolation in upsample rather than bilinear and it performed worse
* Ran upsample with all three layers - performance around the same
* Updated loss function to inverse/reverse Huber, which has been shown to improve robustness and facilitate faster convergence
  * $`D_i`$ is predicted depth & $`D_i^*`$ is ground truth depth
  * $`d_i=\log(D_i)-\log(D_i^*), c=\frac{1}{5}max(d_i)`$
  * Loss: $`\begin{cases} 
          |d_i| & |d_i|\leq c \\
          \frac{d_i^2+c^2}{2c} & |d_i|>c
       \end{cases}`$
  * Performed test with last two layers implementation of architecture

Configuration:
* Batch size = 12
* Learning rate = 1e-4
* Weight decay = 1e-2
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

| Architecture                          | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|---------------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_last_2_layers   (bs12-bicubic) | 26.953  | 0.0935  | 0.253 | 1.959 | 6.065 | 0.299   | 0.652 | 0.866 | 0.948 |
| interp_new2                           | 27.9549 | 0.0934  | 0.206 | 1.586 | 6.814 | 0.304   | 0.657 | 0.864 | 0.946 |
| interp_inv_huber                      | 26.4933 | 0.0876  | 0.201 | 1.518 | 6.493 | 0.288   | 0.681 | 0.874 | 0.953 |

Results (new loss function):
![predicted_inv_huber](uploads/7e32ae408890be43204b084a3a9c9530/predicted_inv_huber.png)
![combined_inv_huber](uploads/a054b6ddfec7b30f3a897c6a6a7ae5b3/combined_inv_huber.png)

### Update 7/7/20 & 8/7/20
Began integrating DenseNet architecture into downsampling
* Using pre-trained (from ImageNet) weights of DenseNet161 - no layers are fixed during training
* Removing classification layer
* Adding skip connections to upsampling architecture (with the exception of the first layer since DenseNet's first convolution downsamples)
* DenseNet chosen over ResNet since it has been shown to produce similar results with considerably fewer parameters - it is overall smaller in size

![NetworkArchitecture-Interp-DenseNet161__1_](uploads/50b30326e5c27fe50e47b195851f6d8f/NetworkArchitecture-Interp-DenseNet161__1_.png)

Configuration:
* Batch size = 6
* Learning rate = 1e-4
* Weight decay = 1e-2
* Epochs = 5 
* Input height x width = 352 x 640
* Output height x width = 352 x 1216 

Metrics:
```console
  silog,   log10, abs_rel,  sq_rel,     rms, log_rms,      d1,      d2,      d3
11.7903,  0.0369,   0.083,   0.372,   3.452,   0.127,   0.918,   0.986,   0.997
```

Visual results:
![predicted_interp_densenet161](uploads/0452129d907c287d49c977f147a3cb69/predicted_interp_densenet161.png)
![combined_interp_densenet161](uploads/ebe8391a301586c5ac4d7c00d4627985/combined_interp_densenet161.png)

Notes:
* High performance, increase in receptive field due to further downsampling 
* Could only use a small batch size due to the number of training weights - overall model file size also large ~645MB (55M parameters)
* Will try with DenseNet121 and also reducing complexity on upsampling by removing convolutions to increase batch size and see the effect on performance
* Potentially do not need to predict depth as deep in upsampling (currently occurring on the second lowest layer) or may want to try deeper 

### Update 9/7/20
Comparison with DenseNet121
* Batch size: 10
* Parameters: 23.8M (280MB model files)
* Modifications to upsampling of DenseNet121 - DenseNet161 performs better but takes considerably longer to train due to small batch size and uses more than double the number of parameters 
  * interp_densenet121mod: Performed addition with convolved concatenation rather than concatenation of skip and upsample, performed worse
  * unet_densenet121: Removed all interpolation - essentially unet structure on upsample
  * interp_densenet121_simplified: No batchnorm/relu on convolution for concatenation

| Architecture                  | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|-------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_densenet121            | 12.3532 | 0.0396  | 0.091 | 0.417 | 3.415 | 0.134   | 0.911 | 0.984 | 0.996 |
| interp_densenet121mod         | 12.4564 | 0.0393  | 0.095 | 0.429 | 3.328 | 0.135   | 0.906 | 0.983 | 0.996 |
| unet_densenet121              | 12.143  | 0.0368  | 0.084 | 0.375 | 3.323 | 0.129   | 0.917 | 0.985 | 0.996 |
| interp_densenet121_simplified | 12.3867 | 0.0379  | 0.087 | 0.408 | 3.448 | 0.132   | 0.91  | 0.984 | 0.997 |

Visual results (DenseNet121):
![predicted_interp_densenet121](uploads/64348d9bbaf405c2dce7e546432fbf21/predicted_interp_densenet121.png)
![combined_interp_densenet121](uploads/811a79e786723b27664f022d55666f7f/combined_interp_densenet121.png)

### Update 10/7/20
Changed upsampling to see if unet did perform better
* Interpolating from the fourth layer had the best result 

| Architecture                   | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|--------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_densenet121_mid         | 12.4816 | 0.0392  | 0.091 | 0.408 | 3.343 | 0.134   | 0.905 | 0.984 | 0.997 |
| interp_densenet121_deep        | 12.5785 | 0.0387  | 0.087 | 0.411 | 3.507 | 0.136   | 0.905 | 0.982 | 0.996 |
| interp_densenet121_third       | 12.5587 | 0.0381  | 0.089 | 0.406 | 3.423 | 0.134   | 0.909 | 0.981 | 0.996 |
| interp_densenet121 (10 epochs) | 11.5503 | 0.0374  | 0.082 | 0.377 | 3.476 | 0.129   | 0.915 | 0.985 | 0.997 |

## Break Week 3
### Update 13/7/20
* Trained unet structure on upsample with DenseNet161 - performed worse than with interpolation 
* Checked DenseNet121 models - overfitting occurred past the 9th epoch
* Tried to continue training DenseNet161 model variant with half the learning rate (5e-05) 
  * At the 7th epoch, values became NaN - need to check what caused this

| Architecture                  | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|-------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| unet_densenet161              | 12.1929 | 0.0371  | 0.084 | 0.38  | 3.365 | 0.13    | 0.914 | 0.984 | 0.996 |
| interp_densenet121 (9 epochs) | 11.5797 | 0.0363  | 0.082 | 0.366 | 3.283 | 0.126   | 0.92  | 0.986 | 0.997 |
| interp_densenet161(6 epochs)  | 11.8187 | 0.0369  | 0.084 | 0.38  | 3.416 | 0.127   | 0.92  | 0.986 | 0.997 |

## Break Week 4
### Update 20/7/20-21/7/20
Transfer learning with DenseNet161 model (6 epochs)
* sd - finetuning all weights
* sd_fixed - only finetuning the focal offset weights
* sd_fixed_conv - finetuning focal offset and first convolution (conv0)
* sd_fixed_tran - finetuning focal offset and first transition layer (transition1)

Capped values between 0-80m for evaluation due to NaN values in predictions

| Architecture                              | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|-------------------------------------------|---------|---------|-------|---------|---------|---------|-------|-------|-------|
| interp_densenet161_sd_fixed   (bs12)      | 46.399  | 0.212   | 1.21  | 1132612 | 213.935 | 0.573   | 0.239 | 0.463 | 0.722 |
| interp_densenet161_sd (bs8)               | 50.0569 | 0.1639  | 0.404 | 6.963   | 13.433  | 0.508   | 0.431 | 0.692 | 0.831 |
| interp_densenet161_sd_fixed_conv   (bs10) | 66.243  | 0.2501  | 0.872 | 27.129  | 21.046  | 0.73    | 0.249 | 0.477 | 0.66  |
| interp_densenet161_sd_fixed_tran   (bs12) | 38.2009 | 0.1363  | 0.336 | 3.458   | 9.696   | 0.4     | 0.451 | 0.756 | 0.905 |

Notes:
Finetuning early layers in DenseNet removed capture of primitive visual features compared to the transition layers

sd_fixed:

![predicted_dn161_sd_fixed](uploads/afe19bbd71b68003d506b01bc2eaeb12/predicted_dn161_sd_fixed.png)![combined_dn161_sd_fixed](uploads/1337ef5f1c19f8a799dd51e501272f5b/combined_dn161_sd_fixed.png)

sd_fixed_conv:

![predicted_dn161_sd_fixed_conv](uploads/3cd5e6d19e9ce3bfb3e051374f2f94ff/predicted_dn161_sd_fixed_conv.png)

sd_fixed_tran:

![predicted_dn161_sd_fixed_tran](uploads/0aac469b5d2499c64ffd2372e48188cc/predicted_dn161_sd_fixed_tran.png)![combined_dn161_sd_fixed_tran](uploads/e5b449dbe697455867ff760a51f24661/combined_dn161_sd_fixed_tran.png)

### Update 22/7/20
Fixed colour normalisation for images for use with DenseNet
*  Faster convergence and higher performance

| Architecture                    | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|---------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| interp_densenet161   (5 epochs) | 11.6627 | 0.0361  | 0.082 | 0.366 | 3.353 | 0.126   | 0.922 | 0.986 | 0.997 |
| interp_densenet121   (4 epochs) | 12.5108 | 0.0386  | 0.09  | 0.412 | 3.433 | 0.134   | 0.909 | 0.982 | 0.996 |

DenseNet161:
![predicted_dn161](uploads/3de2aedad8f2b7140faa69a137508a2f/predicted_dn161.png)
![combined_dn161](uploads/2c0ae6e15b6229d8de45aed8e854063f/combined_dn161.png)

### Update 23/7/20 and 24/7/20
Transfer learning with fixed image normalisation
* No NaN values in predictions with updated normalisation 
* Tried finetuning different transition weights as well as offset (offset, transition1, transition2, transition3)
* Best performance in metrics when finetuning all 3 transition weights and offset - visually, more features captured when first 2 transition weights were finetuned
* Used inverse huber loss to see if there was much difference - metrics improved (to be expected given pixel to pixel calculation), visually similar
* Overlayed prediction on input RGB image to see if features were being picked up differently 

| Architecture                                       | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|----------------------------------------------------|---------|---------|-------|-------|--------|---------|-------|-------|-------|
| dn161_sd_fixed_tran   (bs12, 5 epochs)             | 40.3137 | 0.1443  | 0.362 | 6.554 | 11.667 | 0.427   | 0.429 | 0.735 | 0.893 |
| dn161_sd_fixed_tran (bs12, 10   epochs)            | 38.8772 | 0.1364  | 0.337 | 6.273 | 11.372 | 0.41    | 0.46  | 0.766 | 0.902 |
| dn161_sd_fixed_tran2 (bs12, 5   epochs)            | 35.6285 | 0.1221  | 0.294 | 3.983 | 10.339 | 0.372   | 0.522 | 0.809 | 0.919 |
| dn161_sd_fixed_tran3 (bs12, 5   epochs)            | 34.8794 | 0.1129  | 0.262 | 3.018 | 9.608  | 0.361   | 0.587 | 0.819 | 0.92  |
| dn161_sd_fixed_tran2 (bs12,   10 epochs)           | 35.0376 | 0.1179  | 0.292 | 5.05  | 10.55  | 0.365   | 0.548 | 0.819 | 0.922 |
| dn161_sd_fixed_tran3   (bs12, 5 epochs, inv_huber) | 31.7278 | 0.0999  | 0.237 | 2.55  | 8.846  | 0.326   | 0.639 | 0.855 | 0.937 |
| dn161_sd_fixed_tran2 (bs12, 5   epochs, inv_huber) | 33.9856 | 0.1153  | 0.272 | 2.899 | 9.484  | 0.355   | 0.556 | 0.824 | 0.926 |

**Neighbourhood loss**

Transition1, Transition2, Offset:

![predicted_dn161_tran2_10e](uploads/4341efe00f79c5b9763bb847e40a3b2e/predicted_dn161_tran2_10e.png)
![overlay_dn161_tran2](uploads/a26913683b1f9c18331e05050532cee3/overlay_dn161_tran2.png)

Transition1, Transition2, Transition3, Offset:

![predicted_dn161_tran3](uploads/50fe6b0bf57a0f053d28950b42bf655c/predicted_dn161_tran3.png)

**Inverse Huber Loss**

Transition1, Transition2, Offset:

![predicted_dn161_tran2_ihl](uploads/2372a1734054989cf1e739c2f50d3fbe/predicted_dn161_tran2_ihl.png)
![overlay_dn161_tran2_ihl](uploads/057a4472c17df67a3c272507fd3fafef/overlay_dn161_tran2_ihl.png)

Transition1, Transition2, Transition3, Offset:

![predicted_dn161_tran3_ihl](uploads/5f03e95c2817d485b790b4dc63f16fe6/predicted_dn161_tran3_ihl.png)
![overlay_dn161_tran3_ihl](uploads/c8fd0d063c8b133c8021a297fae3d2d9/overlay_dn161_tran3_ihl.png)

## Break Week 5
### Update 27/7/20 & 28/7/20
* Updated naming conventions for StreetDrone dataset (images names are all zero padded numbers now - easier for sorting and referring to images).
* Conducted cross fold validation (trained on 4 sets, validated on the other) to see overall performance
  * Good for small datasets
  * All sets had similar performance to each other (based off metrics, similar when looking at predictions)

| Architecture                                | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|---------------------------------------------|---------|---------|-------|--------|--------|---------|-------|-------|-------|
| dn161_sd_fixed_tran2 (bs12, 5 epochs, set1) | 30.2124 | 0.1083  | 0.27  | 2.284  | 7.581  | 0.326   | 0.555 | 0.852 | 0.95  |
| dn161_sd_fixed_tran2 (bs12, 5 epochs, set3) | 48.8313 | 0.1674  | 0.516 | 53.284 | 13.567 | 0.519   | 0.432 | 0.692 | 0.842 |
| dn161_sd_fixed_tran2 (bs12, 5 epochs, set4) | 32.2507 | 0.1165  | 0.317 | 10.608 | 9.299  | 0.352   | 0.543 | 0.823 | 0.933 |
| dn161_sd_fixed_tran2 (bs12, 5 epochs, set5) | 32.4527 | 0.1111  | 0.281 | 5.259  | 9.912  | 0.34    | 0.564 | 0.838 | 0.938 |

Drawbacks to note with StreetDrone dataset :
* No data for sky or objects very far away (same issue in KITTI dataset)
* Dataset is very limited - within Monash University and around the same areas (workshop, research way, s5 car park), even between sets a lot of them are quite similar
* Very little traffic on the road - seldom have objects directly in front of the vehicle 
* Sparse dataset (only middle section of most images - not top and bottom)
* Tried to account for imperfections in loss but difficult to determine accuracy 
  * Try to back-calculate camera intrinsics with predictions to see if alignment is better maybe

### Update 29/7/20 - 31/7/20
* Look at focal offset parameter value in KITTI and then StreetDrone models
  * Does change - not sure if proportional to focal length differences in cameras
  * Checked how BTS used only one value for focal length instead of x and y (check KITTI calibration files)
  * Using projection matrix value (fx and fy are approx same)
* Change the neighbourhood size for loss function - 5 instead of 3
  * Worse metrics
  *  Visually similar
* Checked transformations for StreetDrone dataset generation 
  * Extrinsics correct based on StreetDrone gazebo model, however, some sensors were installed manually be the team such as the LiDAR on a custom 3D printed mount. The cameras also differ from the model. 
  * Ran camera calibration again with saved calibration images and had similar results in MATLAB for intrinsics, still difficult to tell where error stems from

## Update 13/8/20
Modified evaluation code for NN
* Cleaning up existing code and separating into a separate script
* Added computation of errors in a neighbourhood fashion (smallest error in a 3x3 pixel window rather than pixel-pixel to account for errors in StreetDrone ground truth data) 
  * Metrics improved (as expected) however the models finetuned with the inverse huber loss performed better than the neighbourhood loss function for the StreetDrone dataset. The neighbourhood loss function uses L2 loss in a neighbourhood sense.

| Architecture                                       | SILog   | Log10 | Rel_abs | Rel2  | RMS   | Log RMS | D1    | D2    | D3    |
|----------------------------------------------------|---------|---------|-------|-------|-------|---------|-------|-------|-------|
| dn161_sd_fixed_tran2   (bs10, 5 epochs, set2, 5n)  | 37.0048 | 0.1084  | 0.262 | 2.826 | 9.154 | 0.343   | 0.575 | 0.838 | 0.933 |
| dn161_sd_fixed_tran2 (bs12, 5   epochs, set2)      | 35.2855 | 0.1097  | 0.26  | 2.701 | 9.193 | 0.344   | 0.568 | 0.833 | 0.932 |
| dn161_sd_fixed_tran2 (bs12,   10 epochs, set2)     | 34.9248 | 0.1037  | 0.25  | 2.607 | 8.914 | 0.331   | 0.6   | 0.846 | 0.938 |
| dn161_sd_fixed_tran2 (bs12, 5   epochs, inv_huber) | 34.1278 | 0.1059  | 0.249 | 2.588 | 9.048 | 0.337   | 0.594 | 0.84  | 0.934 |

Began looking into modelling the camera projection process
* Inputs: LiDAR xyz of every point, estimated depth map at that time period
  * At a given time save xyz of every LiDAR point, rectified camera image, projected LiDAR (for comparison), predict depth map with rectified image and CNN
* Read related papers on using neural networks to determine camera intrinsics and specifically Camera-LiDAR extrinsics 

## Update 21/8/20
Started implementing model to estimate camera parameters
* Used KITTI dataset and transforms
* Was stuck on issues with indices - integers, which are not differentiable, that are used for pixel coordinates
* Resolved issue after discussion with supervisor (trying a different approach)

## Update 28/8/20
Finished implementing model to estimate camera parameters
* Extracted LiDAR data into binary files from StreetDrone data
* Cleaned up code for camera parameter model
* Requires some testing when training different parameters

Wrote a neighbourhood inverse huber loss function for transfer learning
* Have not tested it on StreetDrone data yet 

## Update 4/9/20
Used neighbourhood inverse huber loss function for transfer learning
*  Better performance

Camera calibration network
* Visualised training with combined images
* Tested on slightly larger datasets
* Found that sets 1-2 alignments were fixed, little to no adjustment for 3-5
* Not sure if camera images need to be re-rectified with new parameters - will adjust the fit
* Started training depth NN using adjusted depths for ground truth

Before:

![00034](uploads/a8442eae0ed8506a54d20f354e8f84d2/00034.png)

After:

![00034](uploads/5e86d39efbceb9b2d52b1865cd3a9e3a/00034.png)

## Update 11/9/20
* Tried new loss - value was not always representative of improvement
  * $`D_i`$ is predicted depth & $`D_i^*`$ is ground truth depth
  * $`d_i=\log(D_i)-\log(D_i^*), c=0.2`$
  * Loss: $`\frac{|d_i|}{|d_i|+c}`$
* Parameter values seem to only ever decrease
* Tried manually fixing rigid transformations which didn't make sense
  * Used StreetDrone transformations and then corrected rotation in Z manually using Matlab visualisation of point clouds
* Calibration data with more distortion coefficients - calculated using Matlab 
  * 3 radial distortion coefficients instead of 2 
* When "corrected" works on some images but not others within the same dataset
* Added forward-to-back translation parameter for training

## Update 18/9/20
* Tried calibration when the vehicle is stationary in recordings
* Looked into motion compensation for LiDAR

## Update 25/9/20
* Wrote out math required for motion compensation
![20200925_174907](uploads/8b1c68d79ae3a0c3aecb5b0824304ed1/20200925_174907.jpg)
* Extracted necessary data from rosbags
* Began writing motion compensation code

## Update 2/10/20
* Continued work on motion compensation code
* Added consideration for small angular velocities 
![20201002_194612](uploads/7127f8d7c09fdc6f83fc15685084f48f/20201002_194612.jpg)
*  Having some issues aligning lidar points on the left side of the image, motion compensation is pushing points further to the left (points are being transformed forward in time so this logic should make sense) - this is most noticeable when the vehicle is moving straight with minimal angular velocity

## Update 9/10/20
Shifting timestamps
If moving straight:
* Forward in time - points move outward 
* All lidar pts out of camera --> camera timestamp is earlier than lidar (decrease cam timestamp)
* Right side of image should generally be aligned if timestamps are right
If turning to the right:
* Forward in time - points move to the left
* All lidar pts to the left of camera --> camera timestamp is earlier than the lidar (decrease cam timestamp)
If turning to the left:
* Forward in time - points move to the right
* All lidar pts to the left of camera --> camera timestamp is later than the lidar (increase cam timestamp)
Lidar rotation
* Left side is always behind so points will be shifted further left if moving straight, turning right
* Points will shift to the right if turning left

Figuring out camera timestamp offset - was originally not synced accurately enough
* Pushed cam time back using approx time when car started moving in images and velocity>0 for cam data, increased queue size to 20 and syncing period to 50ms
* Better than before for straight sections and turning generally
* Cam timestamp may be too far back 
* Not sure of stationary alignment - everything seems to be slightly offset to the left (cx is offset more to the left)
* Tried to do motion correction only with lidar (Don't fix camera at all and then try to figure out timestamps)
  * Looked at frames with best alignment and used this to fix camera timestamp - moderately successful 

Result (before on the left, after on the right):
![image](uploads/6bdc73ac3c9c35c77e1ed9f14e959e51/image.png)

## Update 16/10/20
* Integrated motion compensation and calibration into a single neural network - requires further testing at a future date
* Corrected StreetDrone dataset 
  * Using set1, set2 and set4 - corrected calibration and motion where necessary 
* Generated new neural network metrics
  * Improved all metrics