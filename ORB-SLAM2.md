**Table of Contents**

[[_TOC_]]

# Plan
Using the RGB-D (image and depth) functionality of ORB-SLAM2 with single camera images and depth predictions from a NN to estimate pose and mapping (SLAM - simultaneous localisation and mapping). ORB-SLAM2 was chosen as it is one of the top performing RGB-D and monocular SLAM systems that is open source. 

Key tasks:
* Loading KITTI odometry data and depth predictions as pseudo RGB-D data - custom build and config file
* Compare performance with Mono and Stereo ORB-SLAM2 with KITTI dataset (example files already exist for this) - use ATE metric for performance
* Loading StreetDrone data and depth predictions as pseudo RGB-D data - custom build and config file
  * Quantitatively measuring performance may/may not be possible depending on whether full odometry data can be processed from the team's existing datasets (currently being worked on by another team member)
* ROS Pipeline to be a focus later (code does exist)

# Progress

## Update 16/7/20
SLAM:
* [ORB-SLAM2 git repo](https://github.com/raulmur/ORB_SLAM2)
* [ORB-SLAM2 Ros page](http://wiki.ros.org/orb_slam2_ros)
* [ORB-SLAM2 Ros implementation git repo](https://github.com/appliedAI-Initiative/orb_slam_2_ros)

Metrics:
[Computing ATE](https://vision.in.tum.de/data/datasets/rgbd-dataset/tools#:~:text=Absolute%20Trajectory%20Error%20(ATE),truth%20poses%20using%20the%20timestamps.)

Dataset formatting:
* KITTI dataset is rectified and undistorted for odometry purposes
* ORB-SLAM2 takes rectified images
* [TUM](https://vision.in.tum.de/data/datasets/rgbd-dataset/file_formats)
  * 8 bit RGB in PNG format
  * Depth - 16 bit monochrome in PNG format
    * Depth map factor of 5000 to 1m (accounted for in code in Tracking.cc)
* Require timestamps
  * KITTI odometry dataset has timestamps in a file named times.txt
  * See [KITTI mono](https://github.com/raulmur/ORB_SLAM2/blob/master/Examples/Monocular/mono_kitti.cc) and [RGB-D TUM](https://github.com/raulmur/ORB_SLAM2/blob/master/Examples/RGB-D/rgbd_tum.cc) examples for how to load data
* [Associating depth and RGB data](https://vision.in.tum.de/data/datasets/rgbd-dataset/tools)
  * May not be necessary as will write a custom initialisation file based off examples
  * Edit CMakeLists to include in build

## Update 17/7/20
Looked into extracting odometry data for StreetDrone
* Currently, no orientation is available for StreetDrone data (it uses a CAN interface and the IMU itself does not calculate orientation)
* Using another team member's script to extract and publish linear acceleration, angular velocity and magnetic field into topics and then using an open source ROS node, the orientation was found (accuracy unknown at the moment)
  * [IMU tools package git](https://github.com/ccny-ros-pkg/imu_tools/tree/kinetic)
  * [Current filter](http://wiki.ros.org/imu_filter_madgwick)
  * [Rviz visualisation of IMU data](http://wiki.ros.org/rviz_imu_plugin)
* This will not be the final implementation for the team but a means to get odometry data to measure accuracy later - final implementation will be using the CAN interface

Video of final imu outputs
![imu-2020-07-17_19.10.35](uploads/ab762ef4fa54232145b42510fab2659f/imu-2020-07-17_19.10.35.mp4)

## Update 6/8/20
Started using ORB-SLAM2 on the KITTI odometry dataset
* Ran on sequence-00 with Mono and Stereo data
  * Arbitrary scale factor used with Mono as no reference
* ORB-SLAM2 converts all images to grayscale 
* Used ATE scripts from [TUM tools](https://vision.in.tum.de/data/datasets/rgbd-dataset/tools#:~:text=Absolute%20Trajectory%20Error%20(ATE),truth%20poses%20using%20the%20timestamps.)
* Wrote script to convert KITTI trajectories to format used by TUM
* Wrote a script for RGB-D with the KITTI dataset - has not been tested, need to generate depth maps in correct format with depth neural network

Mono (scale 15.5):
```console
ATE RMSE = 8.157857m
```
![seq00_mono](uploads/e0e4365698e8b8c946f8c99e0249d9db/seq00_mono.png)

Stereo: 
```console
ATE RMSE = 1.281890m
```
![seq00_stereo](uploads/551b9cc91ead766c6faefb7d83113b58/seq00_stereo.png)

## Update 10/8/20
Ran RGBD ORB-SLAM2 with KITTI dataset
* Generated depth maps using depth NN
* Tested custom code
* Referenced [paper](https://arxiv.org/pdf/2004.10681.pdf)for how to set baseline for pseudo-RGBD, may need some modification in config file
* Visualisation of trajectories may need to be rewritten as perspective looks strange compared to visualisations in papers with the same KITTI trajectories

Pseudo RGB-D:
```console
ATE RMSE = 6.3m
```
![seq00_rgbd4](uploads/7ac9784707bce572f169f80456888a67/seq00_rgbd4.png)