# Current State
The team is currently using Anaconda3 to create separate virtual environments to accommodate for varying dependencies in neural networks/ code implementations. All Python neural network code written by the team is written in Python 3. 

ROS Kinetic is compatible with Python 2.7 and in particular, issues arise when using OpenCV from Python 3. ROS has its own version of OpenCV in its package path. The current workaround being used is to remove the ROS python path to `import cv2` and then add it back - this is so that rospy can be imported. See similar issues on [forums](https://stackoverflow.com/questions/43019951/after-install-ros-kinetic-cannot-import-opencv). 

A potential solution is Docker. Furthermore, the team believes this could be a better long term method to have a variety of development environments. 

# Useful Docker Resources
*  https://github.com/ACRVMonash/LabWiki/wiki/Using-Docker