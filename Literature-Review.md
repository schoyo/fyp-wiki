# Notes
The Normal  Distributions  Transform: A New Approach to Laser Scan Matching [1]
* NDT Algorithm:
  * First, the 2D space around   the  robot is subdivided regularly  into cells with constant size. Then for each cell, that contains at least three points, the following is done:
    1. Collect all 2D-Points $`x_{i-1...n}`$ contained in this box. 
    1. Calculate the mean $`q=\frac{1}{n}\sum_ix_i`$
    1. Calculate the covariance matrix $`\sum=\frac{1}{n}\sum_i(x_i-q){(x_i-q)}^T`$
    1. The  probability of measuring a sample at 2D-point x contained in this cell is now modeled by the normal distribution N(q,): $`p(x)\text{\textasciitilde}exp(-\frac{{(x-q)}^T\sum^{-1}(x-q)}{2})`$
* Scan alignment algorithm:
  1. Build the NDT of the first scan. 
  1. Initialize the estimate for the parameters (by zero or by using odometry data). 
  1. For  each  sample of the  second  scan:  Map the reconstructed 2D point into the coordinate frame of the first scan according to the  parameters. 
  1. Determine  the  corresponding  normal  distributions for each mapped point.
  1. The  score  for  the parameters  is determined by evaluating  the  distribution  for  each  mapped  point and summing the  result. 
  1. Calculate a new parameter estimate by trying to optimize the score. This is done by performing one step of Newton’s Algorithm. 
  1. Go to 3 until a convergence criterion is  met.

A 3D Scan Matching using Improved 3D Normal Distributions Transform for Mobile Robotic Mapping [2]
* Techniques for concurrent mapping and localisation in 3d space are required
  * Concurrent Mapping and Localisation (CML)
  * Simultaneous Localisation and Mapping (SLAM)
    * Constructing and updating a map of an unknown environment whilst keeping track of agent’s location within it
* Normal Distributions Transform (NDT)
  * A voxel - cubic lattice in a 3d space where the entire space is divided into these cubic lattices 
  * NDT transforms point data in a voxel into a normal distribution 
* Iterative Closest Point (ICP)
  * Computational cost and robustness of NDT are superior to point-to-point based ICP scan matching
  * Searches points closest to each other between two scans and minimises the least square distance using newton’s algorithm - uses raw data of scanned points and references scans as input
  * Computational time increases as the map is extended due to rendering of the reference scan as a whole


A LiDAR Odometry for Outdoor Mobile Robots Using NDT Based Scan Matching in GPS-denied environments [3]
* Two kinds of auxiliary positioning methods: known and unknown environments
* LiDAR data matching methods 
  * ICP: point to point matching or variants: point to feature and feature to feature matching
  * NDT: probabilistic distribution based matching 
* Pipeline of LiDAR odometry (use of data from motion sensors to estimate change in position over time)
  ![Pipeline of LiDAR odometry](uploads/ed85893bf9ad0dddae9711cbd6dd7e36/image6.png)
  * Main time consumption is NDT scan matching, remainder is done in real time

Evaluation of 3D Registration Reliability and Speed – A Comparison of ICP and NDT [4]
* If an initial estimate of odometry is available, local registration is more computationally efficient than global 
* ICP
  * De facto standard 3d registration algorithm used in the robotics community
  * Dominated by map size 
* NDT
  * Model scan is represented using a combination of normal distributions describing the probability of finding part of the surface at any point in space
  * Normal distributions give a piecewise smooth representation of the point cloud so it is possible to apply standard numerical optimisation methods for optimisation
  * Succeeded more frequently than ICP but can fail more drastically in translation and rotation vs only translation in ICP
* NDT with trilinear interpolation
  * NDT limitation: space is subdivided into regular cells (voxels) therefore there are discontinuities in the surface representation at cell edges
  * Weight of each cell’s contribution determined by trilinear interpolation - smoothing effect on data
  * More accurate than NDT, however, execution time increases considerably (450% in experiments) 

An Open Approach to Autonomous Vehicles [5]
* Normal Distribution Transform (NDT) algorithm 
  * Scan matching over 3d point cloud and map data
  * Can be used in 3d or 2d forms and computation cost does not suffer from map size (data quantity) 

Normal Distributions Transform Monte-Carlo Localization (NDT-MCL) [6]
* Monte Carlo Localisation (MCL) 2D
  * Probabilistic map based localisation approach
  * Insufficient accuracy for industrial applications
  * Three steps: prediction, update and resampling
* grid-MCL used the AMCL implementation (ROS package of a probabilistic localization system for a robot moving in 2D)
  * Standard approach uses 2d occupancy grid maps (using noise and uncertain sensor measurement data to generate maps)
* NDT-MCL performed better than grid-MCL with pre-recorded datasets and closed-loop control tests
  * Loss in performance was evident in highly dynamic environments

Autoware on Board: Enabling Autonomous Vehicles with Embedded Systems [7]
* Basic control and data flow of autonomous vehicles
  ![Control and data flow](uploads/19989db65bceb90f136669f5b76e6f39/image8.png)
* Autoware
  * Point Cloud Library: used to manage LiDAR scans and 3d mapping data, perform data filtering and visualisation
  * CUDA: programming framework used for general purpose computing on GPUs
  * Caffe: deep learning framework
  * Real time processing - filters and pre-processes raw point cloud data from LiDAR scanners
  * Scan matching using NDT between 3d maps and downsampled point clouds
    * Mainly uses NDT however, ICP and other localisation/ mapping algorithms can be used
* SLAM
  * Matching process performed against previous LiDAR scan instead of a 3d map
  * Current scan transformed to coordinate system of previous and point cloud is summed - repeated continuously to build the map

Robust Localization Using 3D NDT Scan Matching with Experimentally Determined Uncertainty and Road Marker Matching [8]
* NDT [OFFLINE]
  * Can cope with slight environmental changes since it is represented by a set of normal distributions
  * Convergence performance is dependent on environment appearance and localisation accuracy decreases where there are differences in the map and environment
  * Estimated uncertainty of algorithm 
* Particle filtering (PF) algorithm
  * Road marker matching approaching used where 3D NDT fails
  * Road marker maps are represented as a 2d likelihood map
  * PF receives estimated pose by ND and calculate the likelihood of particles
  * Trajectories are smoother
* Dead reckoning is the process of calculating one's current position by using a previously determined position, or fix, and advancing that position based upon known or estimated speeds over elapsed time and course.

Scan Registration for Autonomous Mining Vehicles Using 3D-NDT [9]
* Further discussion on ICP, 2D-NDT and 3D-NDT
* Approximants to distance function
  * Similar to NDT
    * Describes surface implicitly using quadratic approximants of the squared distance function from the target surface
  * Not compared with other methods - hypothesised performance:
    * May be inappropriate for very noisy data
    * Storage requirements likely larger
* 3D-NDT considerations
  * Better to have an even spatial distribution of points as the sampled subset will have a similar distribution 
  * Cell size - if too large, smaller features will be blurred out but if too small scans may to be too close together for the algorithm to succeed as well as higher memory usage
  * Potentially a different, more adaptive, cell structure would remove this decision

Performance Analysis of NDT-based Graph SLAM for Autonomous Vehicle in Diverse Typical Driving Scenarios of Hong Kong [10]
* Traffic density
  * Accuracy of SLAM was degraded with increased traffic density - largely caused by moving objects (in particular large vehicles which may cover a majority of the LiDAR field of view)
  * Denser traffic introduces more uncertainty
* Urbanisation
  * More features available in sparse and sub-urban areas (buildings, moving objects, trees) compared to dense areas (tall buildings, moving objects) resulting in higher inaccuracy in altitude
* Uncertainty estimation
  * Covariance calculation based on:
  * Degree of matching b/w two consecutive frames of point clouds
  * Time to complete transformation calculations
  * Iteration number required for the Quasi-Newton method to converge
 
ENG: End-to-end Neural Geometry for Robust Depth and Pose Estimation using CNNs [11]
* Single image depth position - NYUv2 (indoor) and KITTI (outdoor) d atasets
* Network architecture - 3 subsystems
  * Depth - largely based on DenseNet151 architecture
  * Flow - estimates optical flow and confidences given an image pair, combined with predicted depth to predict pose, features from FlowNet and FlowNet2.0
  * Camera pose - used iterative and fully connected approaches
    * Iterative: reweighted least squares solver based on residual flow, minimise error function wrt relative transformation
    * Fully connected: network with 3 stacked fully connected layers
* Depth losses: reverse Huber loss function
* RMS metric to determine depth estimation performance
![RMS metric](uploads/f21e8b1a1c384038eba5f0315f754b37/Untitled_picture.png)
* Qualitatively evaluate trajectories using absolute trajectory error and relative pose error 
 
CReaM: Condensed Real-time Models for Depth Prediction using Convolutional Neural Networks [12]
* Nvidia TX2 platform running with custom architecture inspired by ENet and ERFNet
  * LiDAR as ground truth
  * Model compression through knowledge transfer
* NYUv2, RGB-D and KITTI datasets
* RMS metric for depth evaluation
* Real time depth predictions demonstrated with ORB-SLAM2
 
ORB-SLAM: A Versatile and Accurate Monocular SLAM System [13]
* https://github.com/OpenSLAM-org/openslam_orbslam
* Key frame and feature based monocular SLAM
* System
  * ORB - oriented multiscale FAST corners with an associated 256 bit descriptor
  * Run tracking, local mapping and loop closing in parallel
    * More robust and accurate in a large scale environment
  * Bags of words place recognition module to perform loop detection and relocalisation
* Initialising environment - extract ORB features of in current frame and search for matches in reference frame, compute in parallel homography assuming a planar scene and fundamental matrix assuming a nonplanar scene
 
SVO: Semidirect Visual Odometry for Monocular and Multicamera Systems [14]
* Aim to achieve highest accuracy and robustness with limited computational budget
* Aspects for attaining accuracy
  1. long feature tracks with minimal feature drift;
  1. a large number of uniformly distributed features in the image plane; and
  1. reliable association of new features with old landmarks (i.e., loop closures).
* Direct methods - minimise photometric error (pixel level intensities)
  * Require dense/semidense reconstruction of environment
* Feature based methods - minimise reprojection error
  * Extraction of features and descriptors incurs high constant cost per frame
  * Match salient image features in successive frames using invariant feature descriptors
  * VO - incremental estimation of camera pose
  * V-SLAM: detect loop closures and refine large parts of the map
* SVO
  * Features extracted only for select keyframes
  * Direct method used to track features from frame to frame
  * Steps: Sparse image alignment, relaxation, refinement
 
DVL-SLAM: sparse depth enhanced direct visual-LiDAR SLAM [15]
* https://github.com/irapkaist/dvl_slam
* Depth estimation
  * Monocular visual SLAM methods require a bootstrap to initialise the 3d environment - dominates scale error
  * Stereo systems require precise calibration to provide accurate depth estimation
  * LiDAR has relatively small error, vertical sparsity exists in lower cost 3D LiDAR
* Related: Visual-LiDAR SLAM
  * Frame to frame motion estimation combing ICP constraints from LiDAR and 3D-3D, 3D-2D and 2D-2D visual constrains from consecutive stereo images
  * Did not take into account motion distortion of LiDAR points as the sensor moves
  * Expensive tree based data structures required
* Sparse measurements from LiDAR applied to direct method for visual slam
  * Initial motion estimated used a small number of sampled points and directly tracked by intensity in image domain
  * Compensation of LiDAR points by motion based on poses from camera images
  * Current frame added to the sliding window followed by optimisation to improve local accuracy within the window
  * Confirm if position of marginalised keyframe is revisited in a global pose-graph

Real-Time Joint Semantic Segmentation and Depth Estimation Using Asymmetric Annotations [16]
* Semantic segmentation
  * Per-pixel label classification
  * Utilised Light-Weight RefineNet on top of MobileNet-v2
* Asymmetric annotation
  * Using ground truth data from semantic segmentation and depth estimates, updating both branches simultaneously 
  * Noisy estimates in place of missing annotations 
* Single model architecture to include semantic segmentation and depth estimation 
* SemanticFusion SLAM and other dense SLAM systems tested 


# References

*  [1] P. Biber, and W. Strasser, “The normal distributions transform: a new approach to laser scan matching,” Proceedings of the 2003 IEEE/RSJ International Conference on Intelligent Robots and Systems(IROS), Las Vegas, Nevada, USA, 2003, pp. 2743-2748.
* [2] B. Zhou, Z. Tang, K. Qian, F. Fang and X. Ma, "A LiDAR Odometry for Outdoor Mobile Robots Using NDT Based Scan Matching in GPS-denied environments," in 2017 IEEE 7th Annual International Conference on CYBER Technology in Automation, Control, and Intelligent Systems, CYBER 2017, 2018.
* [3] E. Takeuchi and T. Tsubouchi, "A 3-D Scan Matching using Improved 3-D Normal Distributions Transform for Mobile Robotic Mapping".
* [4] J. Saarinen, H. Andreasson, T. Stoyanov and A. J. Lilienthal, Normal Distributions Transform Monte-Carlo Localization (NDT-MCL).
* [5] M. Magnusson, A. Lilienthal and T. Duckett, "Scan registration for autonomous mining vehicles using 3D-NDT," Journal of Field Robotics, 2007.
* [6] M. Magnusson, A. Nüchter, C. Lörken, A. J. Lilienthal and J. Hertzberg, "Evaluation of 3D registration reliability and speed-A comparison of ICP and NDT," in Proceedings - IEEE International Conference on Robotics and Automation, 2009.
* [7] S. Kato, S. Tokunaga, Y. Maruyama, S. Maeda, M. Hirabayashi, Y. Kitsukawa, A. Monrroy, T. Ando, Y. Fujii and T. Azumi, "Autoware on Board: Enabling Autonomous Vehicles with Embedded Systems," in Proceedings - 9th ACM/IEEE International Conference on Cyber-Physical Systems, ICCPS 2018, 2018.
* [8] S. Kato, E. Takeuchi, Y. Ishiguro, Y. Ninomiya, K. Takeda and T. Hamada, "An open approach to autonomous vehicles," IEEE Micro, 2015.
* [9] N. Akai, L. Y. Morales, E. Takeuchi, Y. Yoshihara and Y. Ninomiya, Robust Localization Using 3D NDT Scan Matching with Experimentally Determined Uncertainty and Road Marker Matching.
* [10] W. Wen, L. T. Hsu and G. Zhang, "Performance analysis of NDT-based graph SLAM for autonomous vehicle in diverse typical driving scenarios of Hong Kong," Sensors (Switzerland), vol. 18, no. 11, 2018. 
* [11] T. Dharmasiri, A. Spek and T. Drummond, "ENG: End-to-end Neural Geometry for Robust Depth and Pose Estimation using CNNs," 16 7 2018. 
* [12] A. Spek, T. Dharmasiri and T. Drummond, "CReaM: Condensed Real-time Models for Depth Prediction using Convolutional Neural Networks," 24 7 2018. 
* [13] R. Mur-Artal, J. M. Montiel and J. D. Tardos, "ORB-SLAM: A Versatile and Accurate Monocular SLAM System," IEEE Transactions on Robotics, vol. 31, no. 5, pp. 1147-1163, 1 10 2015. 
* [14] C. Forster, Z. Zhang, M. Gassner, M. Werlberger and D. Scaramuzza, "SVO: Semidirect Visual Odometry for Monocular and Multicamera Systems," IEEE Transactions on Robotics, vol. 33, no. 2, pp. 249-265, 1 4 2017. 
* [15] Y. S. Shin, Y. S. Park and A. Kim, "DVL-SLAM: sparse depth enhanced direct visual-LiDAR SLAM," Autonomous Robots, vol. 44, no. 2, pp. 115-130, 1 1 2020. 
* [16] V. Nekrasov, T. Dharmasiri, A. Spek, T. Drummond, C. Shen and I. Reid, "Real-Time Joint Semantic Segmentation and Depth Estimation Using Asymmetric Annotations," 13 9 2018. 


