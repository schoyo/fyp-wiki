**Table of Contents**

[[_TOC_]]

# Plan
Datasets:
* Have recorded camera and LiDAR data from both vehicles and the transformations between the devices
* KITTI data has both camera and LiDAR with transformations and camera calibrations 
  * Depth maps are also directly available from the KITTI website

Work going forward:
* Datasets with camera and LiDAR with transformations
* Processing so that with every camera image know which points the LiDAR sensor points are and how far they are
  * Image mostly black or 0s and find pixels with LiDAR depth value
  * Image labels with floating point numbers - numpy array or similar
* Complete visual verification checks to ensure labelling is correct

# Progress Updates
## Update 26/3/20
* Written a python class to take in transformations between the camera and LiDAR frames and intrinsic camera calibrations
  * Currently using StreetDrone for this purpose
* Runs concurrently with a ROSBAG, reading in camera and LiDAR data 
  * Uses intrinsic camera calibrations to rectify camera images and find dimensions of image (in pixels)
  * Converts point cloud data into x,y,z coordinates and transforms them into the camera frame
  * Projects from 3d points to 2d plane (camera image) and labels depth
  * Saves camera images to jpg and numpy arrays to csv
* All intrinsic camera calibrations values are for the Sekonix GMSL cameras and were previously determined by other members of the team with a custom checkerboard and Autoware packages
* Transformation values between frames were calculated using the following document by StreetDrone: [sd_twizy.pdf](uploads/f96dbf7f7d6776369d529e0f880444c3/sd_twizy.pdf)

Example of rectified image (before on top, after on bottom):
![cam_orig1](uploads/b250c01dce807f0444f82c885fdf0cbf/cam_orig1.jpg)
![cam_rectified1](uploads/00f666c2e6bcfe3a8a50d41a1c20d9d7/cam_rectified1.jpg)

### References used:
* http://docs.ros.org/kinetic/api/ros_numpy/html/point__cloud2_8py_source.html
* http://docs.ros.org/kinetic/api/tf/html/python/transformations.html
* http://docs.ros.org/kinetic/api/cv_bridge/html/python/
* https://au.mathworks.com/help/vision/ug/camera-calibration.html
* http://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration
* http://docs.ros.org/kinetic/api/sensor_msgs/html/msg/CameraInfo.html
* https://docs.opencv.org/2.4/modules/imgproc/doc/geometric_transformations.html#undistort

## Update 2/4/20
* Fixed transformation between velodyne and camera frames 
  * Changed to a translation then rotation    
* Added visual verification
  * Low to high values (blue to red using jet colormap), 0s for areas without points
* Added option to scale images (currently set to 0.25) to reduce training data file size
* Synced velodyne and camera frames using message_filters
  * Offset camera timestamp in header using rosbag_tools (may be required for multiple StreetDrone bags)

Example of visual verification:

![cam_rectified9](uploads/ef84c780e6e4d4789949c7e8d77319a4/cam_rectified9.jpg)
![depth9](uploads/ecfc775b56b0de8b40cffac2a9e0233c/depth9.jpg)

### References used:
* https://www.programcreek.com/python/example/89433/cv2.applyColorMap
* https://www.ros.org/reps/rep-0103.html
* http://wiki.ros.org/bag_tools
* https://stackoverflow.com/questions/12988151/python-cv2-change-dimension-and-quality
* http://wiki.ros.org/message_filters#ApproximateTime_Policy

## Update 8/4/20
* Combined rectified and depth images to check for discrepancies in transformation
* Adjusted rotation so that depth and rectified images were aligned by visual inspection
  * Still requires further adjustment - focal length and slight rotation
* Had an error with some images becoming smaller/ skewed in dataset - requires further investigation

Examples of combined images after rotation:

![combined5_SE5](uploads/29096b80176aee313f1f537900526f61/combined5.png)
![combined5_workshop](uploads/5b6ea00cba97ab8b3501b19eb14cf01d/combined5.png)

Example of error:

![cam_rectified7](uploads/24c24c67dc6d22e4734a2def28c2c4ba/cam_rectified7.png)

## Update 17/4/20
* Found error with smaller images was caused by compression of data, added in a check to verify image size 
* Updated the intrinsic camera matrix with newer values (collected by another team member)
  * Resulted in a better rectified image (straighter lines)
* Did manual tuning to align depth and rectified images 
  * Rotation and adjustment to focal length used to project image to 2d (did not change focal length used for image correction)
* After discussion with supervisor, will likely use dataset with current adjustments and compensate for errors in loss function. Current errors are too difficult to correct with manual adjustment. 

Example of image rectification:
![cam_rectified6](uploads/1d02121895c04d85ed578e65c53e1f56/cam_rectified6.png)

Examples of verification images:

![combined17](uploads/cbd5ef4b55cc0240a1c0044aff7af97e/combined17.png)
![combined8](uploads/1303058fb29f2b51c514d0364960e3ff/combined8.png)

## Update 23/4/20
* Modified output depth image to same format as KITTI dataset (16bit grayscale png)
  * Image should be able to undergo same processing as KITTI depth data to determine depth
* Fixed time syncing issues in all relevant ROSBAGs
* Created a full StreetDrone dataset for training/validation - can be accessed [here](https://drive.google.com/open?id=1MYUNRt3biric8Efc0Im3B9hpsTsRlJYm)
  * Included verification images - may be useful during training/validation stage 

Example depth image:

![depth151](uploads/4a309414ad48d74e44590f9a347b56f6/depth151.png)

Folder structure is as follows:
```bash
|-- streetdrone_dataset
    |-- 2020-02-24-14-09-23 (227 images)
    |   |-- depth
    |   |-- rectified_images
    |   `-- verification
    |-- 2020-02-24-14-12-34 (655 images)
    |   |-- depth
    |   |-- rectified_images
    |   `-- verification
    |-- 2020-03-05-13-52-19 (481 images)
    |   |-- depth
    |   |-- rectified_images
    |   `-- verification
    |-- 2020-03-05-13-54-12 (1608 images)
    |   |-- depth
    |   |-- rectified_images
    |   `-- verification
    `-- 2020-03-06-14-50-52 (336 images)
        |-- depth
        |-- rectified_images
        `-- verification
```

## Update 8/6/20
* Fixed verification image colours (was not normalised properly previously, and each channel was not accounted for appropriately)
  * Previous verification images will have inaccurate colours representing depth  
* Added calibration file for intrinsic and extrinsic camera values

Fixed verification image example:

![combined3](uploads/985ff59609c2a61b721219833260247b/combined3.png)