**Table of Contents**

[[_TOC_]]

## 25/2/20
* Discussed the focus of my FYP - SLAM using NDT matching, potentially with some visual odometry from cameras. Completed with the Monash Connected Autonomous Vehicle (MCAV) team
  * Normal Distribution Transform scan matching 
  * Sensors available: LiDAR, GPS, IMU and cameras
* Discussed expectations - weekly meetings, access to git repository, have a wiki as a log of work done
* Before next meeting:
  * Read papers suggested previously - CREAM, End-to-end neural geometry, LIE algebra, ORBSLAM, SVO 
  * Write a summary document of MCAV's progress so far in localisation - including some example raw data 
* Meeting again in Week 2 (at the time meetings were not permitted in Week 1)

## 20/3/20 
Ability to work remotely
*  Have access to a powerful computer 

Data density
* Focusing on key points or dense data
* LiDAR data is regularly spaced (more sparse than camera data)
* Camera - depends on key points

Datasets
* Have recorded camera and LiDAR data from both vehicles and the transformations between the devices
* KITTI data may have both camera and LiDAR but the transformations may be unknown

Work going forward
* Datasets with camera and LiDAR with transformations
* Process so that with every camera image know which points the LiDAR sensor points are and how far they are
  * Image mostly black or 0s and find pixels with LiDAR depth value
  * Image labels with floating point numbers - numpy array or similar
  * Store them to disk 
* See where there are discontinuities in depth 
  * Visual verification to ensure labelling is correct 
* Use labelled images as training data for a neural network 
* Loss function on pixels where there is depth 

Readings
* Look into methodologies with dense estimation from cameras 
* End to end papers and similar monocular visual slam based papers
* Neural net reading including PyTorch tutorials, Tensorflow is another option 

Current localisation in MCAV
* Using NDT to create offline maps and then scan matching to localise live
* Could use this as a base for comparison for the neural net

Risk assessment
* Coronavirus inclusion - only if it goes over and above protocols in place
* Under the imagining that work is completed on campus
* All risks associated with autonomous vehicles (including ethics)

Requirements analysis
* Frame it with what we already have in MCAV
* Densify maps, label entities, neural net, look at motion of other bodies (potentially)

Work for next week
* Labelling images 
* Readings
* Risk assessment
* Requirements analysis 

## 27/3/20
Work completed
* Began labelling images using ROSBAGs from StreetDrone
* Found that the KITTI dataset can also be used (transformations and camera calibration data is available) - could do transfer learning and comparison (this dataset will also provide more variety for training)
* Completed basic PyTorch tutorials and did some reading on neural net theory
* Did work on Risk assessment

Verification of images
*  Paint colours to show depth - visual verification much easier than looking at arrays

Potential gotchas 
* Batch norm is a conditioning technique using a sample/batch mean and variance to estimate for the model
  * Be careful as consecutive frames are not independent so want to pull frames from random parts of data
* Have noticed that generated labelled images are quite large in size ~10MB
  * This is due to the high resolution of the images (1280x1920) - way too high for training purposes
  * Suggestion to downsize the data set by at least 4 in each direction 
  * Could pose potential problems later since less images would be used for training on a given network (would have to make network bigger or when up-sampling not going to max resolution)

Neural Nets
* Start with feed forward network that takes a single camera image and attempts to estimate depth with it
* Could later look into recurrent networks which carries memory across time
  * Do some reading on LSTM and GRU
* Plain convolution network -> does convolutions then RELUs and strided convolutions - convolutions on a smaller image have a larger receptive field in the original
  * Begin with RGB and end with depth estimate
* Read papers on "ENG", "CReaM" and  "Real-Time Joint Semantic Segmentation and Depth Estimation Using Asymmetric Annotations"
  * Have a look at the techniques used and think about how they could potentially be utilised in FYP

Work for next week
* Readings
* PyTorch tutorials
* Risk assessment and requirements analysis
* Labelling with visual verification 

## 3/4/20
Requirements analysis
* Expand functional requirements into short paragraphs
* Add a preamble with intro
  * Diagram to show interaction b/w components
* Include criteria to assess system - do not require a rubric
  * Not going to grade on accuracy 
  * Skills brought to design process, testing, improvements
  * Cleanliness/ elegance of design, implementation and clarity
  * Documentation
  * Fit for purpose, real time, usable
* Add constraints
  * E.g. StreetDrone dataset is small so use KITTI, thereby reducing potential usage on physical vehicle

Visual verification
* Some discrepancy between rectified images and depth - potentially rotational offset
* Generate combo image between rectified and depth image so that camera is visible in pixels with '0'
  * Can potentially manually rotate image to fix error
* Separation of camera and LiDAR is front to back 
  * Check translation of a point is closer vs further away
* If issues is centre lines up and edges do not will require further work
  * Edges won't line up at edges in depth dep way, always too far out
* Background lines up but foreground doesn't - translation error, if neither lines up - rotation error
* Discussion of depth estimation improvement methods later
* KITTI dataset already includes depth maps

Issues with using two datasets 
* When training neural net it is implicit that there is learned knowledge about camera parameters
  * Focal length will have changed with two datasets since different cameras so KITTI trained model cannot directly be used on StreetDrone
  * Estimate depth regardless of focal length
    * Direction of ray from camera frame - radial lens distortion, focal length etc. not necessarily needed for FYP but something to consider

Recurrent neural networks
* LSTM and GRU better for sequence analysis such as NLP 
* May be other more beneficial solutions specific to derivatives - have a look

Work for next week
* Create combo image and try remove discrepancy b/w rectified and depth image
* Edit requirements analysis per suggestions
* Risk assessment review
* Readings

## 9/4/20
Visual verification
* Combined depth and rectified images for visual verification and performed some manual adjustment
* Errors
  * Seems to be a forward, backward error 
    * Subtend bigger angle on camera than LiDAR since camera is in front 
  * Small rotational error look at features toward the back of the image
  * Overlapping on both right and left, focal length error
* Tweak rotation (distance) and focal length (LiDAR going too wide, should be shorter by a few percent)
* Have had an issue with some images from the dataset appearing skewed and smaller than the others - requires further investigation 
* Bootstrap later, self consistent for static scene (depth on visual field), can compute camera parameters and transformations 

Dataset
* Ensure images are not highly correlated (not immediately after each other in time)
* Due to limited team dataset train on KITTI and finetune this

Readings
* Neural networks for depth estimation from sparse data 

Neural networks
* Try to implement some existing networks that estimate depth using the KITTI data
* If papers with good performance have any open source code, try those

Work for next week
* Manual tuning of image alignment
* Readings
* Existing neural network implementation 

## 24/4/20
Dataset
-   Generated full StreetDrone dataset - not adjusting any further
    -   Trick with loss function for nearby labelled pixel therefore can be wrong by quite a lot
    -   Adjusted the depth output to be the same as KITTI dataset - 16 bit grayscale
-   \~3300 image pairs in dataset, potentially enough for training

Neural Network
-   Ran an existing convolutional neural network on the team\'s computer
    -   Used existing weights and inferred on the KITTI dataset
    -   Uses DenseNet-161 for down-sampling and local planar assumptions for up-sampling
    -   Can also up-sample with respect to previous layers - several implementations of this around
    -   Try run NN on custom dataset and see performance, try to train on the custom dataset
-   Dataloader issue
    -   When loading data the process hung with one worker
    -   Try Num\_workers=4 --> data always loaded in time for training
-   Sparseness matters when computing loss
    -   For every pixel in ground truth depth, consider neighbouring pixels - this will result in some pixels having more than one loss
        -   Could have a sequence of shifts to add up losses
-   Depth representation considerations
    -   What is representation of ground truth?
        -   True depth, log depth, inverse-depth
    -   Where to put loss
        -   True depth - easier to tell the difference between 1m and 1.5m vs 30m and 30.5m, so objects closer will contribute  more than objects further away --> skews data
        -   Inverse depth - has the opposite effect to true depth, nearby contributes a little, further away contributes more
        -   Log depth - irrelevant of distance away
    -   What does it compute?
        -   Recommend log depth with layer at end of network to compute true depth to pass on
        -   Error calculations on true depth
    -   Recommendation: Compute log depth and loss on log depth
        -   Relative to actual size and independent of object identity
        -   With true depth calculate the log value - mask with 0s and 1s where there is no data
    -   Play around with different combinations to see what works best
-   Easier to visualise final depth map with a ColorMap rather thangrayscale which is the current output

Docker
-   Did some further research
-   Contact Yan about suggestions for setup
-   Better to use docker to keep environments completely separate
-   Python 2.7 vs Python 3 issue with ROS
    -   Could develop NN in python 3 then modify to work in 2.7
    -   Try to separate where possible, hence Docker

Work for next week
-   Try NN on StreetDrone dataset (with current weights and then retrain)
-   Try different representations of depth
-   Look into Docker more

## 1/5/20
Looked into pre-processing
-   Found that the BTS network had restrictions on image sizes - tensor errors
    -   StreetDrone dataset has smaller image sizes than KITTI
-   Recommend a fully convolutional rather than fully connected network to be able to run on different image sizes
    -   Restriction would be that mini batches have to be the same size
    -   Dimensions divisible by some 2\^n based on the number up and down sampled \--\> down and up 3 times so divisible by 8

Build a simpler network to begin with
-   Build a network with a few conv, relus and finally conv no relu
-   Conv at end gives log depth
-   Performance probably poor because receptive field is small
    -   Could add 5x5 or 7x7 strided etc. to help
    -   Depth field at sub resolution and bilinearly interpolate that, gain speed whilst maintaining accuracy
-   For later: U net, big little big with bypass channels
    -   Downsample, process, upsample and pump info across from last time at same resolution
-   Build network that works on KITTI, then figure out how to adapt to work on data
-   Send sequence to build up to in the next day or two

Splitting custom dataset
-   70 training, 30 testing - could be more for training
-   Ensure test data is unseen and does not closely resemble training class
-   Cross fold validation - Hold one set back, train on 4 and test on the fifth, then swap in runs

Docker
-   Started docker setup with advice from Yan

Work for next week
-   Plan a sequence
-   Build a simple fully convolutional network

## 8/5/20
Began implementation of basic CNN discussed earlier in the week
-   Did not have access to team computer to train until today
-   Expect it to perform badly due to low receptive field

Improvements
-   U-net structure will perform better
-   3 or 4 down-samples and 3 or 4 up-samples with concatenations
    -   Conv, relu, conv, relu, strided conv, several times and then up again
-   Be aware of whether log-depth, inverse-depth or true depth is being used for loss, computation and final result

Down-sampling
-   Strided convolutions typically used rather than Maxpool
    -   Maxpool is expressible through ReLUs
-   Tend to have larger kernel size
-   Suggest even number e.g. 4x4 so it overlaps the same on both sides
    -   Conv 4x4, padding 1, stride 2 - 10x10 will go to 5x5
-   Current network is similar to VGG, progress toward 
    -   ResNet - adds back on
    -   DenseNet - concatenate onto channel, strided conv - crushes it down

BatchNorm
-   After every convolution - normalises each channel - requires a large enough mini batch to work
-   Keeps estimate of mean and mean of square
-   Batch of 32, mean of sample is reasonable estimate of dataset with a bit of noise
-   Mean 0, var 1
-   Improves conditioning (speed) and regularisation (generalisation)
-   Typically just before ReLU, adding bias
-   Assumes data is independent (must be careful that data selection is random)

Normalising input
-   RGB is normalised - close to mid-grey
-   Zero mean and unit variance distributed, at least -1 and 1

Loss function masking
-   Add small epsilon to 0s (if finding log of all values and multiplying by mask)

Work for next week
-   Read about more neural network architectures
-   Finish training and inferring the first NN architecture
-   Add in down-sampling and up-sampling to NN

## 15/5/20
Work completed this week
-   Trained and tested basic architecture (convolutions) on KITTI dataset with errors computed
-   Implemented architecture similar to U-net
    -   Trained and tested on KITTI dataset
    -   Tested model trained on KITTI with the StreetDrone dataset

Visualise error map - abs error or squared log error
-   Taking difference and taking loss
-   If pixels do not show up, should be far away

Explore more capacity on downsampling than upsampling
-   Remove one conv layer per downsampling vs doing the same with upsampling
-   May need to rebalance model capacity

Explore DenseNet and ResNet architectures for downsampling

Accounting for different focal lengths
-   Introduce a trainable variable
    -   Adds to log depth (wrong by some scale)
    -   Average log depth error of entire image
    -   Add constant onto log depth - half focal length, twice as big

-   Other methods
    -   Focal length plugged in at the end, log focal length
    -   Radial distortion, add extra channels to input - normalised u and v for every pixel
    -   Cameras have issue called a caustic (normally small) - not actually a pinhole, line is tangent to a curve rather than a point

Dropout - regularisation (good for small datasets to add noise)
-   Multiplicative noise - not good for CNNs since it's aggressive (may get benefit from 5-10% after some BatchNorms)
-   Additive noise instead
    -   In the middle of batchnorm
    -   Remove operations out of batchnorm and make explicit
    -   Noise before bias

Work for next week
-   Explore capacity of downsampling and upsampling
-   Look into accounting for different focal lengths

## 22/5/20
Work summary
-   Compared simplification of up and downsample layers - found simplifying down sampling layers performed worse
-   Added error map verification
-   Added a trainable variable for focal length and trained with KITTI and then StreetDrone dataset

Downsample vs upsample layers
-   More can be done on downsample than upsample (shift upsample convolutional layers to downsample)
-   Observations are in line with previous works

Transfer learning between KITTI and StreetDrone
-   Different loss function only with SD dataset - something more permissive to account for spatial error
    -   For every LiDAR pin - find best pixel within depth search and shift loss image across area and then do min (3x3 or 5x5)
    -   Shift produced depth image - line up along pin - neighbourhood comparison
-   Restrict bits when fine-tuning for StreetDrone
    -   Adaptation to only change the focal length variable for training with StreetDrone
    -   And/Or only finetune first layer filters

Architecture changes
-   Downsample estimate depth, upsample, estimate correction and add on and repeat for upsampling
-   Blurry estimate first and slowly improve
-   Upsample interpolation - torch.nn.functional.interpolate
    -   Bilinear/bicubic
-   Good for generative networks would be interesting to see how it performs in this use case

Overfitting issues/similar
-   Found that earlier epochs performed better in simplified architectures - strange as normally happens with too many variables or not enough data
-   Fixes
    -   Transformations on input and label data
        -   Flip, adding noise to input data
    -   Dropout discussion from last week
-   Learning rate may be too high
    -   Error may drop faster
    -   Reduce learning rate by epoch
-   Ensure use of weight decay

Work for next week
-   Try to resolve overfitting issue
-   Make transfer learning changes
-   Decoding with DenseNet/ResNet
-   Upsample interpolation - if time

## 29/5/20
Work summary
-   Overfitting
    -   Added weight decay
    -   Input data: Random cropping, flipping and colour jitter
-   Tried new architecture
    -   More epochs, bigger batch size
    -   More channels, same epochs and batch size
    -   Performed worse than previous
-   Transfer learning
    -   Loss function - implemented different calculation than discussed - lower results overall
    -   Fixed parameter - lower results

Architecture change
-   Odd that it performed worse
-   Check how it performs if only implemented at half resolution

Downsampling improvement
-   Go straight to DenseNet
-   Append U-net upsample just before classification/ resample
-   Pretrained with ImageNet and randomly initialise upsampling

Transfer learning
-   Novel approach to account for an imperfect dataset
-   Change error metrics too for StreetDrone
-   Loss
    -   Jitter estimated depth (back, forward, up and down)
    -   Estimate how many pixels it\'s offset
    -   Each pos compute loss, min of jitters
    -   Mask value

Scope for remainder of project - requires decision
-   Continue focusing on Depth NN
-   Or shift focus to using depth estimates for SLAM
-   Or use depth with an out of the box SLAM algorithm

Work for next week
-   Determine tasks for remainder of the project and prioritise
-   Progress report
-   Design spec

## 7/8/20
Work I have completed:
-   Architecture
    -   Changed loss function from L2 to reverse huber loss
    -   Changed downsampling architecture to DenseNet121/161
    -   Experimented with variations on upsampling architecture - found the best performing one (does better than base u-net)
-   Transfer learning
    -   Implemented a new loss function for transfer learning - neighbourhood
    -   Experimented with different finetuning different model layers for transfer learning
    -   Conducted cross fold validation with different sets of SD data
-   Depth labelling
    -   Cleaned up depth labelling code and added a README for documentation
-   ORB-SLAM2
    -   Researched ORB-SLAM2 and implementation
    -   Extracted IMU orientation data from some ROSBAG recordings
    -   Started using ORB-SLAM2 on the KITTI odometry dataset - have results for mono and stereo, in the process of testing with depth predictions

Overfitting
-   Model motion - complex to implement, likely not worth it
-   Add a little gaussian noise at lowest resolution, or after each downsampling step

Transfer learning
-   RMS error with regards to best fit in the neighbourhood to get a comparative metric
-   Model camera projection process - learn parameters too
    -   Spatial derivatives over estimated depth image
    -   Here is LiDAR data
        -   Knowns: xyz in LiDAR coord system, relative position from LiDAR to camera
        -   Unknowns: rotation and focal length
    -   Express transformation chain in pytorch, takes LiDAR data and maps it onto image
    -   Differentiable estimate
    -   Calculate derivatives for camera parameters, one at a time
        -   Learn and refine focal length
        -   Forward to backward
    -   Turn everything to derivative on depth error
        -   Derivative of estimated depth

Work for next week
-   ORB-SLAM2 with KITTI predictions
-   Work on derivatives for camera parameters

## 14/8/20

Work completed this week

- ORB-SLAM2 almost working for KITTI data
  - Created KITTI depth predictions
- Implemented neighbourhood metrics for NN
- Read papers about predicting camera calibration parameters and LIE algebra

Transfer learning

- Try neighbourhood loss function using inverse huber rather than L2

Camera parameter estimation

- Capture camera parameters in the pytorch process
- Focus on focal length and forward, backward relative to LiDAR
- Process
  - Use all the LiDAR data - bag with xyz, in pytorch would be an array with vertical - pings, horizontal - xyz
  - Transform to cam coords
  - Put through cam model and normalise to get xy image coord
  - Bilinear interpolation to depth/radar image
    - Values can either be depth (z) or radar (sqrt of x^2+y^2+z^2) of lidar data
    - Grid\_sample function in pytorch for bilinear interpolation
    - Finds sample values and pulls back - for 2d
- Code a small version of this

Work for next week

- Write a basic version of the camera projection process in pytorch

Notes

- List of LiDAR pings, every ping somewhere between 4 pixels, bilinear interp b/w values, gradient

## 21/8/20

Attempted camera parameter model

- Became stuck on pixel coordinates as indices - integers (which are non-differentiable)
- Went over how grid\_sample works
- Using grid\_sample and normalised pixel coordinates (which can be taken as floats) to sample the depth map estimates previously predicted
- Compute loss on sampled depths and projected depths
  - Remember to mask out irrelevant values (not visible)

Using LiDAR data in batches

- Number of points varies so normally difficult to deal with
- Can specify an array size larger than any dataset add pad with LiDAR coordinates that will not be visible (will be masked out later)

Work for next week

- Finish model and try to implement on dataset

## 28/8/20

Completed work

Finished implementing model to estimate camera parameters

- Extracted LiDAR data into binary files from StreetDrone data
- Cleaned up code for camera parameter model
- Requires some testing when training different parameters
- Tested on a small dataset on CPU

Wrote a neighbourhood inverse huber loss function for transfer learning

- Have not tested it on StreetDrone data yet

Testing network

- Set focal length to another value off by percent and then see if it converges back
- Visualise it to check if alignment is working
- May want to check principal point as well
- Test on larger dataset
- May be harder to adjust other properties as they are highly correlated

Make cam model more sophisticated

- Focal length, rotation distortion - cubic/ quantic, initialise to what you want camera to be
- Link back with depth model so that camera calibration can be adjusted to finetune depth estimation

Moving forward

- May want to consider optimising for PX2 so that network runs in real time

Work for next week

- Test network on a larger dataset and visualise
- Test training different parameters

## 4/9/20

Potential reasons for overlapping LiDAR points

- Lidar substantially to the right of camera
  - Large overlap and gap
- Camera is a long way in advance of LiDAR
- Move forward see slightly past the tree - occlusion quite large
- Sanity check for depth, estimate what that gap should be

Projection NN

- Add in a parameter for lidar front to back
- Could add in rotation instead of principal point
  - Vertical axis rotation - yaw issue
  - Most of this should be absorbed by the principal point
- Model does not need to include distortion since projection is onto a rectified/undistorted image so &quot;ideal&quot; pinhole model is alright

Accounting for overlapping in loss

- If lidar is a long way behind where the depth image is, don&#39;t penalise too much - Lidar point is occluded in camera view
- Try absolute error over square error, more than a metre further away or 20% with logs
- Bends other way - abs error v and curves and becomes flat

Changing camera intrinsics but image rectification is not being updated

- Distortion will not change
- This may be an issue - will need to check to confirm if there is a large change in the rectified/undistorted image

Work for next week

- Slowly add in more parameters to see if alignment can be improved further

## 11/9/20

Work completed

- Tried new loss - value was not always representative of improvement
- Values seem to only ever decrease
- Tried manually fixing rigid transformations which didn&#39;t make sense
- Calibration data with different distortion coefficients
- When &quot;corrected&quot; works on some images but not others within the same dataset

Found that certain frames still remained misaligned - largely when turning

- Could be a synchronisation/timestamp issue
- When turning around a roundabout lidar pts all shifted to the left of objects - camera may be capturing before lidar
- Error 1% width of image, turned 1% of a radian
- 10Hz - 1/6 of a 1/10 of s - 15ms for sweep across, 1/2 frame delay
- 20-30 ms about a frame, try shift frames along one-associate with previous lidar sweep (check if left to right or right to left sweep)
- Forward to back parameter accounts for this when moving in a straight line

Try to build in velocity model of car - match lidar sweep with lidar sweep

- Time offset that is unknown
- 15ms approx to get from one side to another
- Require: velocity of vehicle, turning rate
- Move 3d position of lidar pts based on position in sweep
- Time offset\*velocity\*turning rate of vehicle
- Where is it left to right

Parameters only decreasing in value

- Do not want weight decay on those parameters as will likely push values in negative direction

Work for next week

- Look into velocity data of vehicle (if recorded)
- Try training model with no weight decay on parameters

## 18/9/20

Calibration

- Ran calibration model when vehicle is stationary and motion effects should be zero

Correcting lidar so that it is synchronised with camera frame

- Have CAN data - angular velocity and speed (ok since linear velocity is likely in one direction only)

Per point time estimate

- Whole sweep takes 100ms, assume timestamp is end of sweep
- Depending on angle put it back through time - estimate for exact time point was taken
- Theta/360 \*offset

Time sync between LiDAR and CAN data

- Any recent estimate for motion should be okay (closest)

Estimate time offset to camera using the whole frame - then treat points individually

- Transform point
  - Could assume straight line/forward and turn or move along curved path (more accurate)
  - Curve may not be great in time period but check (arc of circle)
  - Rate of rotation and speed so estimate circular arc
  - Radius of curvature - speed/rotation rate, often use inverse radius of radiation so no infinite value if moving straight

Work for midsem break

- Write out math for transformations
- Extract necessary data - CAN, images, LiDAR, timestamps
- Write code
- Start writing final report

## 9/10/20

Work completed

- Added in motion compensation
  - Realised there were overall syncing issues b/w the camera and other data
  - Tried to fix the offset as much as possible using timestamps when the imaged &quot;moved&quot; and velocity\&gt;0
  - Tested on datasets
  - Need to finetune neural network with new data
- Started final report

Motion compensation

- Time offset could also be a parameter - could put the same thing in twice so be careful with that
- Nearly the same - time offset can look like horizontal offset - be aware of drifting

Final report

- Future work section - Feature freeze
  - Shutter triggered exactly
- Include data with enlargements and annotations
- Explain what can go wrong
  - Timestamps, rotation of camera
- Around 50 page mark, lit review 10-20 references
- Flow diagram of software, pictures showing what goes wrong and how they were fixed

Finetune experiment

- Sparse datapoints need to be right

Work for next week

- Final report
- Poster
- Finetune neural network and get new results

- Flow diagram of software, pictures showing what goes wrong and how they were fixed
- Equations - taylor series
- Rolling shutter, shear effect